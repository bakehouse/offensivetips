FROM manda-sphinxbuilder:latest as builder

ADD doc/ /doc

RUN \
umask 077 && \
sh /doc/doc.sh && \
mkdir -p /web && mv /doc/doc/_build/html/* /web/

FROM manda-aiohttpserver:latest 

ADD app/ /app
COPY --from=builder /web /web

USER root:root
RUN chown -R 1500:1500 /web /app
USER 1500:1500
