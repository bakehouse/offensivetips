########################
HTTP Parameter Pollution
########################

.. code-block:: bash

  http://bank/tranfer.php&to=JOHN&to=ATTACKER

| HTTP Parameter Pollution or HPP in short is a vulnerability that occurs due to passing of multiple parameters having same name. 
| There is no RFC standard on what should be done when passed multiple parameters. 
| This vulnerability was first discovered in 2009. 
| HPP could be used for cross channel pollution, bypassing CSRF protection and WAF input validation checks.
| 
| Go to https://en.wikipedia.org/wiki/HTTP_parameter_pollution for backend behaviour matrix

.. raw:: html

  <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/QVZBl8yxVX0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

