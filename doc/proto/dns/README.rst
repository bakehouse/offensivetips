###
DNS
###

****
Enum
****

.. code-block:: bash

  # https://github.com/rbsec/dnscan
  python3 dnscan.py -R '1.1.1.1' -d 'host.com' -n -w /usr/share/SecLists/Discovery/DNS/shubs-subdomains.txt -t 100

|

*****************
DNS Zone Transfer
*****************

.. code-block:: bash

  /usr/bin/dig axfr @{host} -p {port} {domain}

