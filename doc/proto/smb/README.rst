###
SMB
###

| Using share

.. code-block:: bash

   smbclient.py dave@192.168.211.72 -hashes ':08d7a47a6f9f66b97b1bae4178747494'
   # smbclient.py domain/dave:password@192.168.211.72 
   # "shares" to list shares
   # "use myshare" to open share "myshare"
   # cat myfile

|

*****
Mount
*****

.. code-block:: bash

   mkdir /tmp/share
   sudo mount -t cifs -o username='USER',password='PASS',uid=$(id -u),gid=$(id -g),forceuid,forcegid '//DOMAIN/SHARE' '/tmp/share'

|

**********
Range Scan
**********

| Enum basics smb infos or try credentials on IP range

.. code-block:: bash

   netexec smb 192.168.0.0/24
   netexec smb 192.168.0.0/24 -u='User' -p='Password'

|

******************
Recursive Download
******************

.. code-block:: bash

    cd $(mktemp -d)
    smbclient '//1.2.3.4/SYSVOL' -U 'USER%PASSWORD'
    smb: \> mask ""
    smb: \> recurse ON
    smb: \> prompt OFF
    smb: \> cd /
    smb: \> mget *
    smb: \> exit

|

