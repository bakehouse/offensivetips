####
HTTP
####

*******
Fuzzing
*******

.. code-block:: bash

  curl http://TARGET/robots.txt
  curl http://TARGET/sitemap.xml

| https://github.com/danielmiessler/SecLists

.. code-block:: bash

  ffuf -w /usr/share/SecLists/Discovery/Web-Content/raft-small-words.txt -u http://TARGET/FUZZ -fc 404,403 -t 100 -c
  ffuf -w /usr/share/dirb/wordlists/big.txt -u http://TARGET/FUZZ -fc 404,403 -t 100 -c 
  ffuf -w /usr/share/SecLists/Discovery/Web-Content/raft-medium-words.txt -u http://TARGET/FUZZ -fc 404,403 -t 100 -c 
  ffuf -w /usr/share/SecLists/Discovery/Web-Content/raft-small-words.txt -u http://TARGET/FUZZ/v1 -fc 404,403 -t 100 -c
  ffuf -w /usr/share/SecLists/Discovery/Web-Content/raft-small-words.txt -u http://TARGET/api/FUZZ -fc 404,403 -t 100 -c
  ffuf -w /usr/share/SecLists/Discovery/Web-Content/raft-large-words.txt -u http://TARGET/FUZZ -fc 404,403 -t 100 -c 

  # Fuzzing sub domains, filtering reponse size 154
  ffuf -w /usr/share/SecLists/Discovery/DNS/shubs-subdomains.txt -u http://website.com/ -H "Host: FUZZ.website.com" -fs 154 -mc all

  # Custom header
  ffuf -w /usr/share/SecLists/Discovery/Web-Content/raft-medium-words.txt -u -H "Authorization: Basic YWRtaW46Nzg5NDU2" http://192.168.235.201/FUZZ -fc 404,403 -t 100 -c

|

***********
Brute Force
***********

.. code-block:: bash

  ffuf -X POST -H "Content-Type: application/x-www-form-urlencoded" -w /usr/share/SecLists/Usernames/xato-net-10-million-usernames.txt:USER -d 'action=login&email=USER@box.htb' -u http://box.htb/reset.php -t 100 -fs=1761

.. code-block:: bash

  hydra -l user -P rockyou.txt 1.2.3.4 http-post-form "/index.php:fm_usr=user&fm_pwd=^PASS^:Login failed. Invalid"

| Basic auth 

.. code-block:: bash

  patator http_fuzz auth_type=basic url=http://1.2.3.4 user_pass=FILE0:FILE0 0=userpass.txt -x ignore:code=401
  patator http_fuzz auth_type=basic url=http://1.2.3.4 user_pass=FILE0:FILE1 0=/tmp/users 1=rockyou.txt -x ignore:code=401
  patator http_fuzz auth_type=basic url=http://1.2.3.4 user_pass='admin':FILE1 1=rockyou.txt -x ignore:code=401

|

*********
Spidering
*********

.. code-block:: bash

  gospider -s "http://TARGET/" -c 10 -d 0 -t 20

|

*******
Scanner
*******

.. code-block:: bash

  sudo nikto -host http://TARGET

|

************
Certificates
************

.. code-block:: bash

  # Retrieve SSL certificate from server with ip, port and domain
  echo| openssl s_client -connect IP:PORT -servername DOMAINNAME 2>/dev/null | openssl x509 -text

  # For google.com :
  echo| openssl s_client -connect google.com:443 -servername google.com 2>/dev/null | openssl x509 -text

  