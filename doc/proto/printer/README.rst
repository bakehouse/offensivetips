#######
Printer
#######

| https://github.com/RUB-NDS/PRET
| https://book.hacktricks.xyz/network-services-pentesting/515-pentesting-line-printer-daemon-lpd

.. code-block:: bash

    # You need to try ps,pjl,pcl
    python3 ./pret.py laserjet.lan ps

    # HP printers seems to allow pjl
    python3 ./pret.py IP:PORT pjl

|

| Discover printers on network

.. code-block:: bash

    python3 ./pret.py