###
SSH
###

**********
Range Scan
**********

| Try credentials on IP range

.. code-block:: bash

   netexec ssh 192.168.0.0/24 -u='User' -p='Password'

***********
Brute Force
***********

.. code-block:: bash

   hydra -l user -P rockyou.txt -s 22 ssh://1.2.3.4


