###
FTP
###

.. code-block:: bash

  ftp ftp://anonymous:anonymous@1.2.3.4
  # passive

  cme ftp 1.2.3.4 -u'user' -p'pass'

|

*******
Fuzzing
*******

.. code-block:: bash

  /usr/share/patator/patator.py ftp_login host=TARGET user=root password=FILE0 0=./rockyou.txt

.. code-block:: bash

  hydra -l user -P rockyou.txt -f 1.2.3.4 ftp