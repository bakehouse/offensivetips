########
Forensic
########

.. code-block::

  photorec /dev/loop6
|

**********
Volatility
**********

Identify system profile
**********************

.. code-block::

  volatility -f <image> imageinfo

  #sample command line
  volatility -f <image> --profile=<profile plugin>
|

Scan for tcp connection
**********************

.. code-block::

  netscan
|

Command history
**********************

.. code-block::

  cmdscan
|


Identify rogue process
*********************

.. code-block::

  pslist
  pscan
  pstree
|

Check for sign of rootkit
************************

.. code-block::

  psxview
  modscan
  ssdt
  apihooks
|

Extract process,drivers,objects
******************************

.. code-block::

  dlldump
  moddump
  procdump
  memdump  : volatility memdump --dump-dir ./outpu -p 868 #once extracted we can do a "strings" of the fil
  dumpfiles
|

Get verboses informations like username,computer name,data etc 
************************************************************

.. code-block::

  volatility -f <image> --profile=<profile plugin> envars
|

Get password
************
.. code-block::

  volatility -f <image> --profile=<profile plugin> hashdump > file.txt
  #now you can try to brutforce the hash with john or hashcat

|

Find macro and analyze it
************************

.. code-block::

  #1 find the file
  volatility -f <image> --profile=<profile plugin> filescan | grep <file format:doc,pdf ...> #or | grep WIN

  #2 extract the file 
  volatility -f <image> --profile=<profile plugin> dumpfiles -D . -Q 0x0000000<number in the last command>

  #3 search and anlyze macro
  /../../vbaProject.bin #macro are usually here

  # to serach faster for informations
  strings <image> | grep FindProxyForURL -A5

|
