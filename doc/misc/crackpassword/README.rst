#############
Hash Cracking
#############

| Wordlists

.. code-block:: bash

  sudo git clone https://github.com/danielmiessler/SecLists /usr/share/SecLists
  sudo git clone https://github.com/NotSoSecure/password_cracking_rules /usr/share/password_cracking_rules

|

| hashs type : 
|   https://hashcat.net/wiki/doku.php?id=example_hashes
|   https://github.com/HashPals/Name-That-Hash
|

| Hashcat bench 4090 : https://gist.github.com/Chick3nman/32e662a5bb63bc4f51b847bb422222fd
|

**************
HASHCAT SYNTAX
**************

.. code-block:: bash

  # Run without wordlist (try everything from 'a' to complex 9 caracter passwords like '3E~jT#5A!')
  hashcat -m 1000 -i --increment-min=1 -o /tmp/output.txt /tmp/bobhash --username -a 3 -1?l?u?d?s ?1?1?1?1?1?1?1?1?1 --potfile-path=/home/user/HASHCATPOT

|

*********
NT / NTLM
*********

.. code-block:: bash

  echo "5a54f0b5d47e4b245c182f515e9b1d15" >> /tmp/hashes
  hashcat -m 1000 -o /tmp/output.txt /tmp/hashes /usr/share/SecLists/Passwords/Leaked-Databases/rockyou.txt.tar.gz --potfile-path=/home/user/HASHCATPOT

  hashcat -m 1000 -o /tmp/output.txt /tmp/hashes /usr/share/SecLists/Passwords/Leaked-Databases/rockyou.txt.tar.gz -r /usr/share/password_cracking_rules/OneRuleToRuleThemAll.rule --potfile-path=/home/user/HASHCATPOT

|

**********
NTLMv2-SSP
**********

| netNTLMv2 hashs from Responder

.. code-block:: bash

  cat <<'EOF'>/tmp/hash
  user::SERVER:b24064f95cfe57d9:766D1B9860718909004100A5319AA6A8:0101000000000000004931838BBDDA01E8001FD43F67D2A10000000002000800340039003900510001001E00570049004E002D005A005100460034005300480043005A0031003700490004003400570049004E002D005A005100460034005300480043005A003100370049002E0034003900390051002E004C004F00430041004C000300140034003900390051002E004C004F00430041004C000500140034003900390051002E004C004F00430041004C0007000800004931838BBDDA0106000400020000000800300030000000000000000000000000200000617EB18544FCE31F16C38E06301B388D429EC8A7945303FDCF24898F9B2E4D530A001000000000000000000000000000000000000900220063006900660073002F00310030002E00310030002E00310034002E003100320031000000000000000000
  EOF

  hashcat -m 5600 /tmp/hash /usr/share/SecLists/Passwords/Leaked-Databases/rockyou.txt.tar.gz -r /usr/share/password_cracking_rules/OneRuleToRuleThemAll.rule --potfile-path=/home/user/HASHCATPOT

|

****
DOCX
****

.. code-block:: bash

  office2john.py lambda.docx > hash.txt
  hashcat -m 9600 -o /tmp/output.txt hash.txt /usr/share/SecLists/Passwords/Leaked-Databases/rockyou.txt.tar.gz --potfile-path=/home/user/HASHCATPOT

|

***
RAR
***

.. code-block:: bash

  rar2john ARCHIVE.rar >rar_hash
  john rar_hash
  john rar_hash --show

|

******
BCRYPT
******

.. code-block:: bash

  # bcrypt $2*$, Blowfish (Unix)
  hashcat -m 3200 -o /tmp/output.txt '$2y$10$ohq2kLpBH/ri.P5wR0P3UOmc24Ydvl9DA9H1S6ooOMgH5xVfUPrL2' /usr/share/SecLists/Passwords/Leaked-Databases/rockyou.txt.tar.gz --potfile-path=/home/user/HASHCATPOT

|