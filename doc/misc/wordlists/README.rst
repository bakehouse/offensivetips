########
Wordlist
########


******
Static
******

Offline crack

- rockyou.txt (14 million, 133M, 2009)
- crackstation https://crackstation.net/crackstation-wordlist-password-cracking-dictionary.htm

  - crackstation.txt (1212 million, 15G, 2010)
  - crackstation-human-only.txt (64 million, 684M, 2013)


Online bruteforce

- dirb big.txt https://raw.githubusercontent.com/v0re/dirb/master/wordlists/big.txt (20469, no ext)
- web SecList https://raw.githubusercontent.com/danielmiessler/SecLists/master/Discovery/Web-Content/big.txt (20473, no ext)
- others SecList wl https://github.com/danielmiessler/SecLists/tree/master/Discovery/Web-Content
- maybe you can search with target's email on https://haveibeenpwned.com/, it relies on multiple leaks easily found on internet

| others : https://weakpass.com/

*******
Dynamic
*******

.. code-block:: bash

  # Crawl website to extract words 
  sudo cewl -d 2 -w /tmp/docswords.txt https://doc.gamer.wtf

