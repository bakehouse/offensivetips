###########
Pentest ENV
###########

| Preparing pentest env on Ubuntu 24.04.1 LTS
| 

****
Root
****

.. code-block:: bash

  apt update 
  apt upgrade -y
  apt install -y build-essential curl vim
  apt install -y python3-pip pipx git python3-dev 
  apt install -y terminator docker.io golang ruby nginx
  apt install -y mariadb-client-core p7zip-full whatweb nmap php-curl cifs-utils
  apt install -y pidgin mdbtools sipvicious fping hydra freerdp2-x11 python3-ldapdomaindump
  apt install -y hashcat proxychains4 testdisk net-tools default-jdk nikto
  apt install -y nvidia-cuda-toolkit clinfo
  snap install --classic code 

  # Groups
  usermod -aG docker user

  # MSF
  curl https://raw.githubusercontent.com/rapid7/metasploit-omnibus/master/config/templates/metasploit-framework-wrappers/msfupdate.erb > msfinstall && \
  chmod 755 msfinstall && \
  ./msfinstall

  # Wordlist
  mkdir -p /usr/share/wordlists
  git clone https://github.com/danielmiessler/SecLists.git /usr/share/wordlists/SecLists
  ln -s /usr/share/wordlists/SecLists /usr/share/wordlists/seclists
  tar xzvf /usr/share/wordlists/seclists/Passwords/Leaked-Databases/rockyou.txt.tar.gz  -C /usr/share/wordlists/

  # NEO4J
  # https://neo4j.com/docs/operations-manual/current/installation/linux/debian/#debian-installation
  wget -O - https://debian.neo4j.com/neotechnology.gpg.key | gpg --dearmor -o /etc/apt/keyrings/neotechnology.gpg
  echo 'deb [signed-by=/etc/apt/keyrings/neotechnology.gpg] https://debian.neo4j.com stable latest' | tee -a /etc/apt/sources.list.d/neo4j.list
  apt-get update
  add-apt-repository universe
  apt-get install neo4j

  # Services
  systemctl restart neo4j 
  systemctl enable neo4j 
  systemctl restart docker 
  systemctl enable docker 
  systemctl restart nginx
  systemctl enable nginx
  rm /var/www/html/index.nginx-debian.html
  echo 'BeepBoop'>/var/www/html/index.html

|

****
User
****

.. code-block:: bash

  # Golang 
  export PATH="$HOME/go/bin:$PATH"
  echo 'export PATH="$HOME/go/bin:$PATH"'>>~/.bashrc

  # Python
  # pip install --upgrade setuptools --break-system-packages

  # Python env 
  python3 -m venv ~/venv
  . ~/venv/bin/activate

  # Pipx
  pipx ensurepath

  # Rust
  curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
  . "$HOME/.cargo/env"
  rustc --version

  # NetExec
  pipx install git+https://github.com/Pennyw0rth/NetExec
  nxc --version

  # Impacket
  pipx install git+https://github.com/fortra/impacket

  # Ffuf
  go install github.com/ffuf/ffuf/v2@latest

  # Certipy
  pipx install git+https://github.com/ly4k/Certipy
  pipx inject certipy-ad setuptools
  certipy --version

  # BloodyAD
  pipx install git+https://github.com/CravateRouge/bloodyAD

  # BloodHound
  pipx install git+https://github.com/dirkjanm/BloodHound.py

  # PostDL
  pipx install git+https://gitlab.com/charles.gargasson/PostDL.git@master

  # DonPAPI
  pipx install git+https://github.com/login-securite/DonPAPI.git

|

******
Manual
******

.. code-block:: bash

  # NEO4J
  Go to http://localhost:7474/browser/
  creds: neo4j:neo4j
  and change password

  # Bloodhound 
  https://github.com/BloodHoundAD/BloodHound/releases
  # 7z x BloodHound-linux-x64.zip
  # ./BloodHound.bin --no-sandbox