###############
SSH Socks Proxy
###############

| SSH comes with a SOCKS5 proxy feature

****************************
Local Port -> Distant -> Net
****************************

.. code-block:: bash

  ssh -D 127.0.0.1:LOCALPORT usr@DISTANT

****************************
Distant Port -> Local -> Net
****************************

| Use case: execute ssh command from target side, the port shows up on attacker side, then use proxychains to forward requests

.. code-block:: bash

  # Target side
  ssh -R 127.0.0.1:DISTANTPORT usr@DISTANT

  # Attacker side
  echo -e '[ProxyList]\nsocks5 127.0.0.1 8888'>/tmp/TARGET
  proxychains -q -f /tmp/TARGET netexec smb TARGET_NETWORK


***********
Proxy Usage
***********

.. code-block:: bash

  # CURL
  curl --socks5 HOST:PORT https://ip.offensive.run

  # APT
  apt-get -o Acquire::socks::proxy "socks5://HOST:PORT" update


  # PROXYCHAINS4
  #   Install : 
  #   $ apt-get install proxychains4
  #   Then replace default "socks4 127.0.0.1 9050" with "socks5 HOST PORT" into /etc/proxychains4.conf
  proxychains4 curl https://ip.offensive.run


  # PIP
  python3 -m pip --proxy socks5:HOST:PORT install --upgrade aiohttp


