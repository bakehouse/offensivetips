
####
HTTP
####

Basic HTTP Server
*****************

.. code-block:: bash

  # Serve current dir with default http module
  sudo python3 -m http.server --cgi 80

