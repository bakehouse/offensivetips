###
Git
###

Gitea server
************

.. code-block:: bash

    cat <<'EOF'>/tmp/gitea.yml
    version: "3"

    networks:
      gitea:
        external: false

    services:
      server:
        image: gitea/gitea:1.22.3
        container_name: gitea
        environment:
          - USER_UID=1000
          - USER_GID=1000
        restart: always
        networks:
          - gitea
        volumes:
          - /tmp/gitea:/data
          - /etc/timezone:/etc/timezone:ro
          - /etc/localtime:/etc/localtime:ro
        ports:
          - "3000:3000"
          - "222:22"
    EOF

    docker-compose -f /tmp/gitea.yml up

|