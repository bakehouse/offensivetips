Offensive Tips
==============

*****
Intro
*****

| Offensive Tips, Exploits
| Git: https://gitlab.com/charles.gargasson/offensivetips

.. image:: https://media.giphy.com/media/l46C6sdSa5DVSJnLG/giphy.gif
  :width: 600

| Sources ❤️ :
| - https://book.hacktricks.xyz/
| - https://www.ired.team/
| - https://www.thehacker.recipes/
| - https://github.com/swisskyrepo/PayloadsAllTheThings
| - https://beta.hackndo.com/
| - https://mayfly277.github.io/
| - https://byt3bl33d3r.github.io/
| - https://0xdf.gitlab.io/
| - https://otter.gitbook.io/red-teaming
|

***********
Methodology
***********

| The "did you try ?" list

New Target
**********

- Full TCP port scan
- Check for SNMP

New Service
***********

- Search for service version, and google it !! (service xxx v1.2.3 exploit git cve poc)
- Search related OS info, ALWAYS try to ask google for kernel version exploit !!

New Website
***********

- Check URLs for other vhosts
- Scan dirs (wfuzz/fuff/dirb/gobuster)
- Check requests with ZAP/Burp, especialy on user inputs and forms
- Default passwords

New Credentials
***************

- Try it everywhere and with differents protocols (rdp,ssh,smb,rpc,winrm)
- Try password mutation (usr1_srv => usr2_srv)

New System Account
******************

- Run enumeration scripts (linpeas/winpeas)
- Look for sudo entries and check them on https://gtfobins.github.io/
- ALWAYS check services path for secrets (ex : /var/www/html)

|
|
|
|
|
|











.. meta::
   :http-equiv=Cache-Control: no-cache, no-store, must-revalidate
   :http-equiv=Pragma: no-cache
   :http-equiv=Expires: 0

.. toctree::
   :hidden:
   :maxdepth: 10
   :glob:
   :caption: # Network stuff

   network/***/README*

.. toctree::
   :hidden:
   :maxdepth: 10
   :glob:
   :caption: # Protocols

   proto/***/README*

.. toctree::
   :hidden:
   :maxdepth: 10
   :glob:
   :caption: # Windows Ecosystem

   winecosystem/***/README*

.. toctree::
   :hidden:
   :maxdepth: 10
   :glob:
   :caption: # Linux

   linux/***/README*

.. toctree::
   :hidden:
   :maxdepth: 10
   :glob:
   :caption: # Apps & SVC

   apps/***/README*

.. toctree::
   :hidden:
   :maxdepth: 10
   :glob:
   :caption: # WEB Generic

   webgeneric/***/README*

.. toctree::
   :hidden:
   :maxdepth: 10
   :glob:
   :caption: # Misc

   misc/***/README*

.. toctree::
   :hidden:
   :maxdepth: 10
   :glob:
   :caption: # ROGUE SERVER

   rogue/***/README*

.. toctree::
   :hidden:
   :maxdepth: 10
   :glob:
   :caption: # C2 / TRANSMISSION

   c2servs/***/README*   

.. toctree::
   :hidden:
   :maxdepth: 10
   :glob:
   :caption: # PIVOTING

   pivoting/***/README* 


.. toctree::
   :hidden:
   :maxdepth: 10
   :glob:
   :caption: # RESSOURCES

   ressources/***/README* 

.. toctree::
   :hidden:
   :maxdepth: 10
   :glob:
   :caption: # Passive Recon

   passivrecon/***/README*
