#########
Port Scan
#########

.. code-block:: bash

  # RustScan for super fast TCP port scans :)
  sudo docker run --rm -it rustscan/rustscan:latest -g -b 3000 -a 1.2.3.4
  sudo docker run --rm -it rustscan/rustscan:latest -g -b 1000 --scan-order "Random" -p21,22,23,25,53,80,88,123,137,139,389,443,445,631,1434,3306,3389,5900,5432,8080 -a 192.168.1.0/24

  # masscan: Range of IP
  sudo masscan -p80 1.2.3.0/24 --rate=1000 -e tun0 --router-ip 10.11.0.1

  # masscan: Range of ports
  sudo masscan -p1-65535 1.2.3.4 --rate=1000 -e tun0 --router-ip 10.11.0.1

.. code-block:: bash

  # Scan ports for version
  sudo nmap -v -A -sV -T4 --min-rate 1000 --min-parallelism 10 -n -Pn -p445,631,80,53,443 1.2.3.4

  # SNMP
  sudo nmap -p161,162 -sU -PN -n 1.2.3.4
  snmpbulkwalk -c public -v2c 1.2.3.4 . | tee /tmp/snmp_pub
  # snmpwalk -c public -v1 1.2.3.4
  
.. code-block:: bash

  # PING SWEEP (PING SCAN)
  fping -g -a -q 192.168.1.0/24
  sudo nmap -n -sP 192.168.1.0/24

.. code-block:: bash

  # sudo nmap --script-updatedb
  sudo nmap -sV -p 443 --script "vuln" 1.2.3.4
