#######
LXC/LXD
#######

******
Basics
******

.. code-block:: bash

  # Init LXD after installation (first time)
  lxd init --minimal

  # Run new container
  lxc launch ubuntu mycontainer

  # List launched containers
  lxc list

  # Stop/Start container
  lxc stop mycontainer
  lxc start mycontainer

  # Exec command
  lxc exec mycontainer -- /bin/bash

  # Export container
  lxc publish mycontainer --alias mycontainer
  lxc image export mycontainer /tmp/

  # Import container 
  lxc image import /tmp/*.tar.gz --alias mycontainer


**********
Privileges
**********

.. code-block:: bash

  # Create container with security privilege
  lxc init ubuntuone mycontainer -c security.privileged=true

  # Mount host folder to container 
  lxc config device add mycontainer foobar disk source=/ path=/mnt/root recursive=true



