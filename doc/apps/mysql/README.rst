#####
MySQL
#####

******
Basics
******

.. code-block:: bash

  mysql -h SRV -D DBNAME -u USR -e 'select * from TABLE;' --password=PASS

******
SQLMAP
******

.. code-block:: bash

  sqlmap -d mysql://USR:PASS@SRV:3306/DBNAME -D DBNAME -T TABLE --dump --flush-session --batch



*****
OTHER
*****

.. code-block:: bash

  # Read file (cat for correct render)
  mysql -h SRV -D DBNAME -u USR -e 'select load_file("/etc/passwd")' --password=PASS | echo -e "$(cat)"

