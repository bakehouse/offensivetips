############
AWS Products
############

*********
S3 Bucket
*********

.. code-block:: bash

  # Upload file to bucket
  aws s3 --endpoint-url http://s3bucketserver/ cp localfile.txt s3://bucketname/distantlocation.txt

  # List file(s)
  aws s3 --endpoint-url http://s3bucketserver/ ls bucketname/distantlocation.txt


***********
S3 Dynamodb
***********

.. code-block:: bash

  # List Dynamodb tables
  aws dynamodb list-tables --endpoint-url http://s3dynamodb/

  # Dump table infos
  aws dynamodb scan --table-name tablename --endpoint-url http://s3dynamodb/

  # Create table (https://docs.aws.amazon.com/fr_fr/amazondynamodb/latest/developerguide/SQLtoNoSQL.CreateTable.html)
  aws dynamodb --endpoint http://s3dynamodb/ create-table --table-name Music \
    --attribute-definitions AttributeName=Artist,AttributeType=S AttributeName=SongTitle,AttributeType=S \
    --key-schema AttributeName=Artist,KeyType=HASH AttributeName=SongTitle,KeyType=RANGE \
    --provisioned-throughput ReadCapacityUnits=1,WriteCapacityUnits=1

  # Add table item
  aws dynamodb --endpoint http://s3dynamodb/ put-item --table-name Music \
    --item '{ "Artist": {"S": "Darude"}, "SongTitle": {"S": "Sandstorm"} }'
