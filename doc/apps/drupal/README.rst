######
Drupal
######

.. code-block:: bash

    docker run --rm droope/droopescan scan drupal -u http://10.129.179.196/ --enumerate v

|

| Exploits
| https://www.exploit-db.com/exploits/41564
| https://github.com/dreadlocked/Drupalgeddon2