#############
Visual Studio
#############

| We can weaponize visual studio project SLN file to execute commands when opening and/or building
| https://github.com/charlesgargasson/SLN2RCE
| https://github.com/cjm00n/EvilSln
| https://github.com/moom825/visualstudio-suo-exploit
