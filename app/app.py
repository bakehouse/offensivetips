from aiohttp import web
import asyncio

import logging
logging.basicConfig(level=logging.DEBUG)

async def debug(request):
  return web.Response(text="Hey!")

async def nomatch(request):
  return web.Response(text="")

async def index(request):
    return web.FileResponse("/web/index.html")

def get_app():
  app = web.Application()

  # Defines Routes 
  g = web.get # GET Method
  p = web.post # POST Method
  s = web.static # Static files

  app.add_routes([
    g('/debug', debug),
    g('/', index),
    s('/', '/web/', show_index=False),
    g('/{tail:.*}', nomatch)
  ])
  
  return app
